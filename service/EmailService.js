var nodemailer = require('nodemailer');
var Config = require('./Config.js');
var EmailTemplate = require('email-templates').EmailTemplate;
var smtpTransport = require('nodemailer-smtp-transport');
var path = require('path');
var moment = require('moment');
var keystone = null; 
var User = null;
var Appointment = null;
const ical = require('ical-generator');
var appointmentTemplate = new EmailTemplate(path.join(__dirname, 'templates', 'confirmationEmail'));
var alertTemplate = new EmailTemplate(path.join(__dirname, 'templates', 'alert'));
var paymentAlertTemplate = new EmailTemplate(path.join(__dirname, 'templates', 'paymentAlert'));
var paymentReceiptTemplate = new EmailTemplate(path.join(__dirname, 'templates', 'paymentReceipt'));
var calendarInviteTemplate = new EmailTemplate(path.join(__dirname, 'templates', 'calendarInvite'));
var calendarCancelTemplate = new EmailTemplate(path.join(__dirname, 'templates', 'calendarCancel'));
var transporter = nodemailer.createTransport(smtpTransport(Config.mail.smtp));
const createReceipt = require('./invoice').createReceipt;
const fs = require("fs");

function sendCalenderMail(appointment, user, method, cbk) {
	console.log('Sending calendar invite for appointment ' + JSON.stringify(appointment));
	Appointment.model.findOne({_id: appointment.id}).populate('workCategories').exec((err, appointment) => {
		if (err) {
			console.error(err);
			return cbk(err, null);
		}
		User.model.findOne({_id : user}).exec((err, recepient) => {
			if (err){
				console.error(err);
				return cbk(err, null);
			}
			else {
				console.log('Sending calendar invite to technician ' + JSON.stringify(recepient) + ' for appointment ' + JSON.stringify(appointment));
				var startDate = appointment.date;
				startDate.toLocaleString('en-US', { timeZone: 'America/Chicago' })
				startDate.setHours(8 + appointment.slot);
				var endDate = appointment.date;
				endDate.toLocaleString('en-US', { timeZone: 'America/Chicago' })
				endDate.setHours(8 + appointment.slot + 1);
				var cal = ical({domain: Config.domain, name: 'Mr Repairman Appointment'});
				var template = method === 'request' ? calendarInviteTemplate : calendarCancelTemplate;
				template.render({appointment: appointment, 
					date:moment(appointment.date).format('dddd[,] MMMM Do'), 
					time : moment().hour(9).minute(0).add(appointment.slot, 'hours').format('h:mm a'), 
					recepient: recepient, 
					workCategories: appointment.workCategories.map((category)=> {
						return category.name;
					})}, function(err, result){
						if (err){
							console.error(err);
						}
						else {
							cal.createEvent({
								start: startDate,
								end: endDate,
								summary: Config.mail.appointment.description + ' at ' + appointment.addressLine1 + ', ' + appointment.addressLine2 ,
								description: result.html,
								location: appointment.addressLine1 + ', ' + appointment.addressLine2,
								url: 'http://' + Config.domain
							});
							transporter.sendMail({
								from: Config.mail.template.from + ' <' + Config.mail.smtp.auth.user + '>',
								to: recepient.email,
								bcc: Config.mail.alertTo,
								subject: Config.mail.appointment.description,
								html: result.html,
								text: Config.mail.appointment.description + ' at ' + appointment.addressLine1 + ', ' + appointment.addressLine2,
								icalEvent: {
									method: method,
									content: cal.toString()
								}
								}, function(err, responseStatus) {
								if (err) {
									console.error(err);
									console.error('Error occured sending calendar invite for appointment ' + JSON.stringify(appointment) + ' to technician ' + JSON.stringify(recepient));
								} 
								else {
									console.log('Successfully send calendar invite for appointment ' + JSON.stringify(appointment));
								}
								cbk(err, responseStatus);
							});	
						}	
				});
			}
		});
	});
}

module.exports = {
	init: (service) => {
		keystone = service;
		User = keystone.list('User');
		Appointment = keystone.list('Appointment');
		ReferenceData = keystone.list('ReferenceData');

	},

	sendAppointmentConfirmationMail: function(appointment, payment, cbk){
		console.log('Sending confirmation email for appointment ' + JSON.stringify(appointment));
		var cnf = {};
		ReferenceData.model.find({name: 'per-hour-rate'}).exec((err, data)=> {
			cnf['perHourRate'] = data[0].value;
			ReferenceData.model.find({name: 'service-fee'}).exec((err, data)=> {
				cnf['serviceFee'] = data[0].value;
				ReferenceData.model.find({name: 'paid-for-hours'}).exec((err, data)=> {
					cnf['paidForHours'] = data[0].value;
					appointmentTemplate.render({config: cnf, appointment: appointment, payment: payment, 
						date:moment(appointment.date).format('dddd[,] MMMM Do'), 
						time : moment().hour(9).minute(0).add(appointment.slot, 'hours').format('h:mm a')}, 
						function(err, result){
						if (err){
							console.error(err);
							return cbk(err, null);
						}
						User.model.find({isAdmin: true}).exec((err, users) => {
							var mailOptions = {
								from: Config.mail.template.from + ' <' + Config.mail.smtp.auth.user + '>',
								to: appointment.email,
								bcc: Config.mail.alertTo,
								subject: 'Mr Repairman Appointment Confirmation', 
								html: result.html
							};
							transporter.sendMail(mailOptions, function(err, res){
								if (err){
									console.error(err);
								}
								cbk(err, res);
							});	
						});
					});
				});
			});
		});
	},

	sendAlert: function(data, cbk){
		msg = JSON.stringify(data);
		alertTemplate.render(msg, function(err, result){
			if (err){
				console.error(err);
				return cbk(err, result);
			}
			var mailOptions = {
				from: Config.mail.template.from + ' <' + Config.mail.smtp.auth.user + '>',
				to: Config.mail.alertTo,
				subject: Config.mail.template.from + ': Alert', 
				html: result.html
			};
			transporter.sendMail(mailOptions, function(err, res){
				if (err){
					console.error(err);
				}
				cbk(err, res);
			});
		});
	},

	sendPaymentAlert: function(data, cbk){
		console.log('Sending payment alert email ' + JSON.stringify(data));
		paymentAlertTemplate.render(data, function(err, result){
			if (err){
				console.error(err);
				return cbk(err, null);
			}
			var mailOptions = {
				from: Config.mail.template.from + ' <' + Config.mail.smtp.auth.user + '>',
				to: Config.mail.alertTo,
				subject: Config.mail.template.from + ': Payment notification', 
				html: result.html
			};
			transporter.sendMail(mailOptions, function(err, res){
				if (err){
					console.error(err);
				}
				cbk(err, res);
			});
		});
	},
	
	cancelAppointment: function(appointment, user, cbk) {
		if (!user){
			return;
		}
		sendCalenderMail(appointment, user, 'CANCEL', cbk);
	},

	sendAppointment: function(appointment, user, cbk) {
		if (!user){
			return;
		}
		sendCalenderMail(appointment, user, 'request', cbk);
	},

	sendResetPasswordMail: function(user, cbk){
	
	},

	sendInvoice: function(invoice, payment, to, cbk) {
		console.log('Sending invoice..');
		if (!fs.existsSync('./public/documents')){
			console.log('creating new dir ./public/documents for the invoice..');
			fs.mkdirSync('./public/documents');
		}
		if (!fs.existsSync('./public/documents/receipts')){
			console.log('creating new dir ./public/documents for the invoice..');
			fs.mkdirSync('./public/documents/receipts');
		}
		let path = './public/documents/receipts/';
		let dir = path + invoice.name;
		if (!fs.existsSync(dir)){
			console.log('creating new dir ' + dir + 'for the invoice..');
			fs.mkdirSync(dir);
		}
		console.log('dir ready');
		let fileName = dir + '/MrRepairmanReceipt-' + payment.name + '.pdf';
		let writeS = createReceipt(invoice, payment, fileName);
		writeS.on('finish', function(){
			console.info('finished');
			paymentReceiptTemplate.render({payment:payment}, function(err, result){
				if (err){
					console.error(err);
					return cbk(err, null);
				}
				var mailOptions = {
					from: Config.mail.template.from + ' <' + Config.mail.smtp.auth.user + '>',
					to: to?to:invoice.appointment.email,
					bcc: Config.mail.alertTo,
					subject: Config.mail.template.from + ': Payment Receipt', 
					html: result.html,
					attachments: [
						{  
							filename: 'MrRepairmanReceipt-' + payment.name + '.pdf',
							content: fs.createReadStream(fileName)
						}
					]
				};
				payment.receipt = Config.protocol + '://' + Config.domain + '/documents/receipts/' + invoice.name + '/MrRepairmanReceipt-' + payment.name + '.pdf'; 
				payment.save();
				transporter.sendMail(mailOptions, function(err, res){
					if (err){
						console.error(err);
					}
					cbk(err, res);
				});
			});			
		});
	}
};
