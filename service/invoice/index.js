const fs = require("fs");
const PDFDocument = require("pdfkit");
const htmlToText = require('html-to-text');

const getAmount = (amount) => {
	return amount ? amount : 0;
}

const lineGap = 20;

function createReceipt(invoice, payment, path) {
  console.log('creating receipt');
  let doc = new PDFDocument({ margin: 50 });

  generateHeader(doc);
  generateCustomerInformation(doc, invoice, payment);
  let position = generateInvoiceTable(doc, invoice);
  generateFooter(doc, position);

  doc.end();
  let writeS = fs.createWriteStream(path) ;
  doc.pipe(writeS);
  return writeS;
}

function generateHeader(doc) {
  doc
    .image("./service/invoice/logo.png", 50, 40, { width: 150 })
    .fillColor("#444444")
    .fontSize(10)
    .text("Mr Repairman", 200, 40, { align: "right" })
    .text("3023 N Clark St, Suite 292", 200, 55, { align: "right" })
    .text("Chicago, IL 60657", 200, 70, { align: "right" })
    .font("Helvetica-Bold")
    .text("www.mrrepairman.com", 200, 85, { align: "right" })
    .moveDown();
}

function generateCustomerInformation(doc, invoice, payment) {
  console.log('generateCustomerInformation : invoice.name :' + invoice.name + ', payment.amount:' + payment.amount);
  doc
    .fillColor("#444444")
    .fontSize(18)
    .text("Receipt", 50, 110);

  generateHr(doc, 115);

  const customerInformationTop = 150;

  doc
    .fontSize(10)
    .text("Invoice Number:", 50, customerInformationTop)
    .font("Helvetica-Bold")
    .text(`${invoice.name}`, 150, customerInformationTop)
    .font("Helvetica")
    .text("Payment ID:", 50, customerInformationTop + 15)
    .font("Helvetica-Bold")
    .text(`${payment.name.toUpperCase()}`, 150, customerInformationTop + 15)
    .font("Helvetica")
    .text("Payment Date:", 50, customerInformationTop + 30)
    .text(formatDate(new Date()), 150, customerInformationTop + 30)
    .font("Helvetica-Bold")
    .text("Payment Amount:", 50, customerInformationTop + 45)
    .text(
      formatCurrency(payment.amount),
      150,
      customerInformationTop + 45
    )
    .font("Helvetica-Bold")
    .text(invoice.appointment.customerName.first + ' ' + invoice.appointment.customerName.last, 300, customerInformationTop)
    .font("Helvetica")
    .text(invoice.appointment.addressLine1, 300, customerInformationTop + 15)
    .text(invoice.appointment.addressLine2 + ", Zip Code: " +invoice.appointment.zipCode + ", ",300,customerInformationTop + 30)
    .text(invoice.appointment.email, 300, customerInformationTop + 45)
    .font("Helvetica-Bold")
    .text(invoice.appointment.phoneNumber, 300, customerInformationTop + 60)
    .moveDown();

  generateHr(doc, 220);
}

function generateInvoiceTable(doc, invoice) {
  
  let position = 270;
  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    position,
    "Item",
    "Description",
    "",
    "",
    "Amount"
  );
  generateHr(doc, position);

  position = position + 30;
  doc.font("Helvetica");

  const materials = htmlToText.fromString(invoice.materials, {
    wordwrap: 550
  });

  position = position + 30;
  generateTableRow(
    doc,
    position,
    'Materials',
    materials,
    "",
    "",
    formatCurrency(getAmount(invoice.materialCost))
  );

  let offset = materials.length === 0 ? 0 : 60;

  generateLightHr(doc, position + offset);

  position = position +  30 + offset;
  
  const details = htmlToText.fromString(invoice.details, {
    wordwrap: 550
  });

  generateTableRow(
    doc,
    position,
    'Labor',
    details,
    "",
    "",
    formatCurrency(getAmount(invoice.laborCharge))
  );
  
  offset = details.length === 0 ? 0 : 60;

  generateLightHr(doc, position + offset);

  position = position + 30 + offset;
  generateTableRow(
    doc,
    position,
    'Service Charge',
    "",
    "",
    "",
    formatCurrency(getAmount(invoice.serviceCharge))
  );
  generateLightHr(doc, position);

  position = position + 30;
  generateTableRow(
    doc,
    position,
    'Van Charge',
    "",
    "",
    "",
    formatCurrency(getAmount(invoice.vanCharge))
  );

  generateLightHr(doc, position);

  position = position + 30;
  generateTableRow(
    doc,
    position,
    'Disposal',
    "",
    "",
    "",
    formatCurrency(getAmount(invoice.disposalCharge))
  );
  
  generateLightHr(doc, position);

  let subTotal = getAmount(invoice.materialCost) 
    + getAmount(invoice.laborCharge) + getAmount(invoice.serviceCharge) + getAmount(invoice.vanCharge)
    + getAmount(invoice.disposalCharge);

  position = position + 30;

  generateTableRow(
    doc,
    position,
    '',
    "",
    "Initial Payment",
    "",
    "-"+formatCurrency(getAmount(invoice.initialPayment))
  );

  position = position + 25;
  generateTableRow(
    doc,
    position,
    '',
    "",
    "Discounts",
    "",
    "-"+formatCurrency(getAmount(invoice.discountAmount))
  );

  position = position + 20;
  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    position,
    '',
    "",
    "Total",
    "",
    formatCurrency(getAmount(invoice.totalAmount))
  );

  doc.font("Helvetica");
  position = position + 20;
  generateTableRow(
    doc,
    position,
    '',
    "",
    "Paid To Date",
    "",
    "-"+formatCurrency(getAmount(invoice.paidAmount))
  );

  generateLightHr(doc, position);

  doc.font("Helvetica-Bold");
  position = position + 30;
  generateTableRow(
    doc,
    position,
    '',
    "",
    "Balance",
    "",
    formatCurrency(invoice.balance)
  );

  return position;
}

function generateFooter(doc, position) {
  doc
    .fontSize(10)
    .text(
      "Thank you for your business.",
      50,
      730,
      { align: "center", width: 500 }
    );
}

function generateTableRow(
  doc,
  y,
  item,
  description,
  unitCost,
  quantity,
  lineTotal
) {
  doc
    .fontSize(10)
    .text(item, 50, y)
    .fontSize(description.length < 15 ? 10: 7)
    .text(description, 150, y)
    .fontSize(10)
    .text(unitCost, 280, y, { width: 90, align: "right" })
    .text(quantity, 370, y, { width: 90, align: "right" })
    .text(lineTotal, 0, y, { align: "right" });
}

function generateCostRow(
  doc,
  y,
  item,
  lineTotal
) {
  doc
    .fontSize(10)
    .text(item, 50, y)
    .text(lineTotal, 0, y, { align: "right" });
}


function generateHr(doc, y) {
  y = y + lineGap;
  doc
    .strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(560, y)
    .stroke();
}


function generateLightHr(doc, y) {
  y = y + lineGap;
  doc
    .strokeColor("#ddddddd")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(560, y)
    .stroke();
}

function formatCurrency(usd) {
  return "$" + (usd).toFixed(2);
}

function formatDate(date) {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return year + "/" + month + "/" + day;
}

module.exports = {
  createReceipt
};