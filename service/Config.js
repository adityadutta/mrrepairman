/**
 * Created by Aditya on 11/17/2015.
 */
module.exports = {
    apiAuth: process.env.NODE_ENV === 'production',
    signupExpiryDays: 1,
    jwtSecret: 'jx454dj5gf45hj46hj4343',
    jwtExpiryInSecs: 10080,
    minPasswordLength: 6,
    maxPasswordLength: 50,
    serviceFee: 75,
    paidForHours: 1,
    laborRate: 150,
    message: {
        generalError: 'Woops, something bad happened and we are taking a look at it',
        emailBad: "That email doesn't seem right.",
        badEmailorUsername: "That email or username doesn't seem right.",
        emailExists: "This email is already registered.",
        verifyEmail: "We have sent you a verification mail. Please check your mail and verify.",
        authGeneralError: "Snap. Something went wrong with authorization.",
        missingPassword: "Oh no, you haven't set a password yet!",
        usernameTaken: "Sorry, this username is already taken.",
        passwordInvalid: "The provided password is invalid!",
        passwordWrong: "Whoa, that password wasn't quite right!",
        usernameMissing: "You need to supply a username",
        emailMissing: "Please provide your email",
        passwordBadLength: "Password must be between 5 and 20 letters",
        signupSucess: "Sign up successfull"
    },
    mail: {
        alertTo: 'aditya@beescribe.com',
        smtp: {
          service: 'Gmail',
          auth: {
              user: 'contact@mrrepairman.com',
              pass: 'cqnukuzsqkhiyaym'
          }
        },
        template: {
            from:'Mr Repairman',
            subject: ''
		},
		appointment : {
			description: 'Mr Repairman Appointment'
        },
         /*
        smtp: {
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                type: 'OAuth2',
                user: 'david@mrrepairman.llc',
                clientId: '65718949097-uvb6pgeg3l6sbl8bvj22hh5bgnvs25ru.apps.googleusercontent.com',
                clientSecret: 'vDC6R1OAAxF1KGMGxUFvt04y',
                refreshToken: '1/UDrwh2iBIvIg1KG4XTO9K2U7fY-tUg7RNYraIwLPvhTPk2BUTfWNTCgvIT1f5PXi',
                accessToken: 'ya29.GludBjKPfrWCdMa3ctYgaYPWvH2ifiUCeyzzmozOUwkU1nfHA2G_MSahGOLFlB1VGAUwWfN1M0rq87giEiXmbHWK-9au18LiiHZ7pm45bzoQrvEU8bH4w7ZGHFr6'
            }
        },
        */
	}, 
    domain : process.env.NODE_ENV === 'production'? 'mrrepairman.com': 'localhost:' + process.env.PORT ,
    protocol: process.env.NODE_ENV === 'development'? 'http': 'https' ,
    appointmentIdStart: 1278,
    invoiceIdStart:1893
};
