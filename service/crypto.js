const Config = require('./Config');
const algorithm = 'aes-256-ctr';
const crypto = require('crypto');

module.exports = {
    encrypt: function encrypt(text){
        var cipher = crypto.createCipher(algorithm,Config.jwtSecret);
        var crypted = cipher.update(text,'utf8','hex')
        crypted += cipher.final('hex');
        return crypted;
    },

    decrypt: function decrypt(text){
        var decipher = crypto.createDecipher(algorithm,Config.jwtSecret)
        var dec = decipher.update(text,'hex','utf8')
        dec += decipher.final('utf8');
        return dec;
    }
}
