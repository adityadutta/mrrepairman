const keystone = require('keystone');
const Appointment = keystone.list('Appointment');
const Payment = keystone.list('Payment');
const Invoice = keystone.list('Invoice');
const Testimonial = keystone.list('Testimonial');
const Slide = keystone.list('Slide');
const ZipCode = keystone.list('ZipCode');
const Project = keystone.list('Project');
const WorkCategory = keystone.list('WorkCategory');
const ReferenceData = keystone.list('ReferenceData');
const User = keystone.list('User');
const braintree = require('braintree');
const cors = require('cors');
const middleware = require('./middleware');
const moment = require('moment');
const importRoutes = keystone.importer(__dirname);
const csvFilePath='./routes/Zip_Codes.csv';
const csv=require('csvtojson');
const EmailService = require('./../service/EmailService');
EmailService.init(keystone);
const Config = require('./../service/Config');
const mongoose = require('mongoose');
	
let zipCodeData = {};
csv()
.fromFile(csvFilePath)
.on('json',(jsonObj)=>{
	zipCodeData[jsonObj.ZIP] = parseCoordinates(jsonObj.the_geom)
})
.on('done',(error)=>{
	if (error){
		console.error(error);
	}
	console.log('end')
})

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

let environment =  braintree.Environment.Sandbox;
let merchantId = process.env.BRAINTREE_MERCHANT_ID_SANDBOX;
let publicKey = process.env.BRAINTREE_PUBLIC_KEY_SANDBOX;
let privateKey = process.env.BRAINTREE_PRIVATE_KEY_SANDBOX;

if (keystone.get('env') === 'production') {
	environment =  braintree.Environment.Production;
	merchantId = process.env.BRAINTREE_MERCHANT_ID;
	publicKey = process.env.BRAINTREE_PUBLIC_KEY;
	privateKey = process.env.BRAINTREE_PRIVATE_KEY;
}

var gateway = braintree.connect({
	environment: environment, 
	merchantId: merchantId,
	publicKey: publicKey,
	privateKey: privateKey
  });

  
function parseCoordinates(val){
	val = val.substring(val.indexOf('(((') + 3, val.indexOf(')))'));
	let vals = val.split(',');
	let coords = vals.map((val)=> {
		val = val.trim();
		let cords = val.split(' ');
		return {lng: parseFloat(cords[0]), lat: parseFloat(cords[1])};
	});
	return coords;
}

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
};

// Setup Route Bindings
exports = module.exports = function (app) {
	console.log('Keystone running with env : ' + keystone.get('env'));
	
	if (keystone.get('env') === 'development'){
		app.use(cors());
	}

	app.get('/api/config', (req, res)=> {
		console.log('received request for /api/config..');
		WorkCategory.model.find()
		.exec(function(err, categories) {
			ZipCode.model.find()
			.exec(function(err, zipCodes) {
				res.send({workCategories: categories, zipCodes: zipCodes});
			});
		});
	});

	app.post('/api/checkZipCode', (req, res)=> {
		console.log('received request for /api/checkZipCode..');
		let zipCode = parseInt(req.body.zipCode);
		console.log('looking for zip code..' + zipCode);
		ZipCode.model.find({name: zipCode})
		.exec(function(err, zipCodes) {
			console.log('found zip codes ' + JSON.stringify(zipCodes));
			if (zipCodes.length) {
				res.send({service: true, data : zipCodeData[zipCode] })
			}
			else {
				res.send({service: false, data : zipCodeData[zipCode]});
			}
		});
	});

	app.get('/api/testimonials', (req, res)=> {
		console.log('received request for /api/testimonials..');
		Testimonial.model.find({})
		.exec(function(err, testimonials) {
			console.log('responsding with ' + testimonials.length + ' testimonials.');
			res.send(testimonials);
		});
	});

	app.get('/api/projects', (req, res)=> {
		console.log('received request for /api/projects..');
		Project.model.find({})
		.exec(function(err, projects) {
			console.log('responsding with ' + projects.length + ' projects.');
			res.send(projects);
		});
	});

	app.get('/api/slides', (req, res)=> {
		console.log('received request for /api/slides..');
		Slide.model.find({})
		.exec(function(err, slides) {
			console.log('responding with ' + slides.length + ' slides.');
			res.send(slides);
		});
	});

	app.get('/api/workCategories', (req, res)=> {
		console.log('received request for /api/workCategories..');
		WorkCategory.model.find({})
		.exec(function(err, workCategories) {
			console.log('responding with ' + workCategories.length + ' workCategories.');
			res.send(workCategories);
		});
	});

	app.get('/api/refdata/:name', (req, res) => {
		console.log('/api/refdata called for ' + JSON.stringify(req.params.name));
		ReferenceData.model.find({name: req.params.name}).exec((err, data)=> {
			res.send(data);
		});
	});

	app.post('/api/checkout/appointment', (req, res) => {
		console.log('/api/checkout called for ' + JSON.stringify(req.body));
		let data = req.body.user;

		WorkCategory.model.find({name : { "$in" : data.workCategories }}).exec().then((cats) => {
			apt = new Appointment.model({date: new Date(data.appointmentDate), slot: data.appointmentSlot, 
				time: moment().hour(9).minute(0).add(data.appointmentSlot, 'hours').format('h:mm a'),
				customerName : {
					first: data.firstName,
					last: data.lastName
				},
				addressLine1: data.addressLine1, 
				addressLine2: data.addressLine2, 
				zipCode: data.zipCode, 
				phoneNumber: data.phoneNumber.trim().replace(/-/g, ""), 
				email: data.email, 
				customerInstructions: data.message, 
				workCategories: cats			
			});
			
			apt.save((err, appointment) => {
				if (err){
					console.error(err);
					EmailService.sendAlert({error: err, info: 'Failed to save appointment while scheduling. Input data : ' + JSON.stringify(req.body)}, function(res, err){
						
					});
					return res.status(500).send(err);
				}
				else {
					ReferenceData.model.find({name: 'service-fee'}).exec((err, data)=> {
						console.log('service fee :' + data[0].value ) ;
						const amount = data[0].value 
						gateway.transaction.sale({
							amount: amount,
							paymentMethodNonce: req.body.paymentMethodNonce,
							options: {
								submitForSettlement: true
							}
						}, (err, result) => {
							if (err){
								console.error(err);
								appointment.notes = 'PAYMENT FAILED :' + JSON.stringify(err);
								payment = new Payment.model({date: new Date(), 
									appointment: appointment,
									amount: amount,
									successful: false,
									error: err.toString()
								});
								payment.save();
								appointment.payment = payment._id;
								appointment.save();
								EmailService.sendAlert({error: err, info: 'Payment failed while paying for appointment. Input data : ' + JSON.stringify(req.body)}, function(res, err){
							
								});
								res.status(500).send(err);	  
							}
							else {
								console.log('payment successful for ' + JSON.stringify(req.body) + ' with response ' + JSON.stringify(result));
								payment = new Payment.model({date: new Date(), 
									name: result.transaction.id,
									appointment: appointment,
									amount: amount,
									successful: true,
									error: null
								});
								payment.save();
								
								appointment.payment = payment._id;
								appointment.save();
								EmailService.sendAppointmentConfirmationMail(appointment, payment, function(err, emailRes){
									if (err) {
										res.send({email: false, appointment: appointment});
									}
									else {
										res.send({email: true, appointment: appointment});
									}
								});
							}						
						});
					});
				}
			});
		})
		.catch((err)=> {
			console.log(err);
			EmailService.sendAlert({error: err, info: 'workcat find failed for categories ' + JSON.stringify(data.workCategories)}, function(err, emailRes){
						
			});
			return res.status(500).send(err);
		});		
	});

	app.get("/api/client_token", function (req, res) {
		if (gateway) {
			gateway.clientToken.generate({}, function (err, response) {
			  if (err){
			  	console.log(err);
				EmailService.sendAlert({error: err, info: 'client_token failed '}, function(err, emailRes){
						
				});
			  	res.status(500).send(err);
			  }
			  else {
				  res.send(response.clientToken);
			  }
			});			
		}
		else {
			EmailService.sendAlert({error: err, info: 'client_token failed '}, function(err, emailRes){
						
			});
			res.status(500);
		}
	});

	app.get("/login", function (req, res) {
		res.redirect('/keystone');
	});

	app.get("/admin", function (req, res) {
		res.redirect('/keystone');
	});

	app.get("/employee", function (req, res) {
		res.redirect('/');
	});

	app.get('/api/futureappointments', (req, res) => {
		console.log('/api/futureappointments called for ' + JSON.stringify(req.body));
		var dt = new Date();
		//dt.setYear(dt.getYear()-1);
		var user = req.user;
		let query = {date: {'$gte': dt}};
		
		Appointment.model.find(query)
		.exec(function(err, appointments) {
			if (err){
				console.log(err);
				EmailService.sendAlert({error: err, info: 'failed to retrieve future appointments for user ' + JSON.stringify(user)}, function(err, emailRes){
						
				});
			}
			else {
				console.log('responsding with ' + appointments.length + ' future appointments.');
				res.send(appointments);
			}
		});
	});

	app.get('/api/appointments', Config.apiAuth ? middleware.requireUser : middleware.testUser, function(req, res){
		console.log('/api/appointments called for ' + JSON.stringify(req.body));
		var dt = new Date();
		dt.setYear(dt.getYear()-1);
		var user = req.user;
		let query = {date: {'$gte': dt}};
		if (Config.apiAuth && !user.isAdmin) {
			query = {date: {'$gte': dt}, 'assignedTo': user._id};
		}

		Appointment.model.find(query).populate('invoices').populate('assignedTo')
		.exec(function(err, appointments) {
			if (err){
				console.log(err);
				EmailService.sendAlert({error: err, info: 'failed to retrieve appointments for user ' + JSON.stringify(user)}, function(err, emailRes){
						
				});
			}
			else {
				console.log('responsding with ' + appointments.length + ' appointments.');
				res.send(appointments);
			}
		});
	});

	app.get('/api/user', Config.apiAuth ? middleware.requireUser : middleware.testUser, function(req, res){
		console.log('/api/user called for ' + JSON.stringify(req.body));
		res.send(req.user);
	});

	app.get('/api/users', (req, res) => {
		console.log('/api/user called for ' + JSON.stringify(req.body));
		User.model.find({isAdmin: false}).exec((err, users)=> {
			res.send({user:req.user, users: users});
		});
	});

	app.post('/api/invoice/agreement', Config.apiAuth? middleware.requireUser : middleware.testUser, (req, res) => {
		console.log('/api/invoice/agreement called with request ' + JSON.stringify(req.body));
		let inInvoice = req.body;
		Invoice.model.findOne({name: inInvoice.name}).populate('appointment').exec((err, invoice) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'failed to find invoice during invoice agreement. Input data was ' + JSON.stringify(req.body)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}
			else {
				invoice.agreementContent = inInvoice.agreementContent;
				invoice.agreementSignatureData = inInvoice.agreementSignatureData;
				invoice.lastUpdate = new Date();
				invoice.save((err, inv) => {
					res.send(inv);
				});
			}
		});
	});

	app.post('/api/invoice/sessions', Config.apiAuth? middleware.requireUser : middleware.testUser, (req, res) => {
		console.log('/api/invoice/sessions called with request ' + JSON.stringify(req.body));
		let inInvoice = req.body;
		Invoice.model.findOne({name: inInvoice.name}).populate('appointment').exec((err, invoice) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'failed to find invoice during invoice sessions. Input data was ' + JSON.stringify(req.body)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}
			else {
				invoice.sessions = inInvoice.sessions;
				invoice.lastUpdate = new Date();
				invoice.save((err, inv) => {
					res.send(inv);
				});
			}
		});
	});

	app.post('/api/invoice/details', Config.apiAuth? middleware.requireUser : middleware.testUser, (req, res) => {
		console.log('/api/invoice/details called with request ' + JSON.stringify(req.body));
		let inInvoice = req.body;
		Invoice.model.findOne({name: inInvoice.name}).populate('appointment').exec((err, invoice) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'failed to find invoice during invoice details. Input data was ' + JSON.stringify(req.body)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}
			else {
				invoice.details = inInvoice.details;
				invoice.lastUpdate = new Date();
				invoice.save((err, inv) => {
					res.send(inv);
				});
			}
		});
	});

	app.post('/api/invoice/materials', Config.apiAuth? middleware.requireUser : middleware.testUser, (req, res) => {
		console.log('/api/invoice/materials called with request ' + JSON.stringify(req.body));
		let inInvoice = req.body;
		Invoice.model.findOne({name: inInvoice.name}).populate('appointment').exec((err, invoice) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'failed to find invoice during invoice materials. Input data was ' + JSON.stringify(req.body)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}
			else {
				invoice.materials = inInvoice.materials;
				invoice.lastUpdate = new Date();
				invoice.save((err, inv) => {
					res.send(inv);
				});
			}
		});
	});

	app.post('/api/invoice', Config.apiAuth? middleware.requireUser : middleware.testUser, (req, res) => {
		console.log('/api/invoice called with request ' + JSON.stringify(req.body));
		let inInvoice = req.body;
		Invoice.model.findOne({name: inInvoice.name}).populate('appointment').exec((err, invoice) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'failed to find invoice during invoice materials. Input data was ' + JSON.stringify(req.body)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}
			else {
				invoice.sessions = inInvoice.sessions;
				invoice.details = inInvoice.details;
				invoice.materials = inInvoice.materials;
				invoice.materialCost = inInvoice.materialCost;
				invoice.laborCharge = inInvoice.laborCharge;
				invoice.serviceCharge = inInvoice.serviceCharge;
				invoice.vanCharge = inInvoice.vanCharge;
				invoice.disposalCharge = inInvoice.disposalCharge;
				invoice.discountAmount = inInvoice.discountAmount;

				invoice.lastUpdate = new Date();

				invoice.save((err, inv) => {
					res.send(inv);
				});
			}
		});
	});

	app.post('/api/checkout/invoice', (req, res) => {
		console.log('/api/checkout/invoice called for ' + JSON.stringify(req.body));
		let inInvoice = req.body.invoice;
		Invoice.model.findOne({name: inInvoice.name}).populate('appointment').exec((err, invoice) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'failed to find invoice while paying invoice. Input data was ' + JSON.stringify(req.body)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}	
			else {
				invoice.agreementContent = inInvoice.agreementContent;
				invoice.agreementSignatureData = inInvoice.agreementSignatureData;	
				invoice.sessions = inInvoice.sessions;
				invoice.details = invoice.details;
				invoice.materials = inInvoice.materials;
				invoice.materialCost = inInvoice.materialCost;
				invoice.laborCharge = inInvoice.laborCharge;
				invoice.serviceCharge = inInvoice.serviceCharge;
				invoice.vanCharge = inInvoice.vanCharge;
				invoice.disposalCharge = inInvoice.disposalCharge;
				invoice.discountAmount = inInvoice.discountAmount;
				invoice.lastUpdate = new Date();

				gateway.transaction.sale({
					amount: req.body.amount,
					paymentMethodNonce: req.body.paymentMethodNonce,
					options: {
						submitForSettlement: true
					}
				}, 
				(err, result) => {
					if (err){
						console.error(err);
						EmailService.sendAlert({error: err, info: 'Payment failed while paying for invoice. Input data : ' + JSON.stringify(req.body)}, function(err, emailRes){
						
						});
						payment = new Payment.model({
							date: new Date(), 
							invoice: invoice,
							amount: req.body.amount,
							successful: false,
							customerSignatureData: inInvoice.customerSignatureData,
							error: err.toString()
						});

						payment.save();
						if (!invoice.payments) {
							invoice.payments = [];
						}
						invoice.payments = invoice.payments.concat([payment._id]);
						invoice.save();
						res.status(500).send(err);
					}
					else {
						console.log('payment successful for ' + JSON.stringify(req.body) + ' with response ' + JSON.stringify(result));
						payment = new Payment.model({
							date: new Date(), 
							invoice: invoice,
							name: result.transaction.id,
							amount: req.body.amount,
							customerSignatureData: inInvoice.customerSignatureData,
							successful: true
						});
						payment.save((err, payment) => {
							if (err){								
								console.error(err);
								EmailService.sendAlert({error: err, info: 'Error occured while saving payment object in database. Input data : ' + JSON.stringify(req.body)}, function(err, emailRes){
						
								});					
							}
							else {
								if (!invoice.payments) {
									invoice.payments = [];
								}
								invoice.payments = invoice.payments.concat([payment._id]);
								invoice.save((err, invoice) => {
									if (err){
										console.error(err);
										EmailService.sendAlert({error: err, info: 'Error occured while saving invoice object in database. Input data : ' + JSON.stringify(req.body)}, function(err, emailRes){
						
										});							
									}
									else {
										res.send(invoice);
										EmailService.sendInvoice(invoice, payment, null, function(err, emailRes){
										});
									}
								});
							}
						});
					}
				});
			}
		});
	});

	app.get('/api/appointment/:id', Config.apiAuth ? middleware.requireUser : middleware.testUser, (req, res)=> {
		console.log('received request for /api/appointments..');
		Appointment.model.findOne({_id: mongoose.Types.ObjectId( req.params.id)}).populate('invoices').exec((err, appointment) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'Failed to retrieve appointment. Input data : ' + JSON.stringify(req.params)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}
			res.send(appointment);
		});
	});

	app.post('/api/trigger', (req, res) => {
		console.log('/api/trigger called for ' + JSON.stringify(req.body));
		let trigger = req.body;
		if (trigger.key==='s9234jwjwed309'){
			console.log('key accepted');
			if (trigger.function === 'regenerateInvoice'){
				console.log('regenerateInvoice');
				Payment.model.findOne({_id: mongoose.Types.ObjectId( trigger.payment)}).populate('invoice').exec((err, payment) => {
					if (err){
						console.error(err);								
					}
					else {
						Invoice.model.findOne({_id: payment.invoice._id}).populate('appointment').exec((err, invoice) => {
							EmailService.sendInvoice(invoice, payment, trigger.mail, function(err, emailRes){
								if (err){
									console.error(err);								
								}
							});
						});
					}		
				});
			}
		}
		res.send(trigger);
	});

	app.get('/api/invoiceid/:id', (req, res)=> {
		console.log('received request for /api/invoiceid..');
		Invoice.model.findOne({_id: mongoose.Types.ObjectId(req.params.id)}).populate('appointment').exec((err, invoice) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'Failed to retrieve invoice. Input data : ' + JSON.stringify(req.params)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}
			res.send(invoice);
		});
	});

	app.get('/api/invoice/:name', Config.apiAuth ? middleware.requireUser : middleware.testUser, (req, res)=> {
		console.log('received request for /api/invoice..');
		Invoice.model.findOne({name: req.params.name}).populate('appointment').exec((err, invoice) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'Failed to retrieve invoice. Input data : ' + JSON.stringify(req.params)}, function(err, emailRes){
						
				});
				return res.status(500).send(err);
			}
			res.send(invoice);
		});
	});

	app.post('/api/getLastAppointment', (req, res)=> {
		let email = req.body.email;
		let phoneNumber = req.body.phoneNumber;

		console.log('received request for /api/getLastAppointment..' + JSON.stringify(req.body));
		if (email && phoneNumber){
			console.log('ddd');
			Appointment.model.findOne({email: email, phoneNumber: phoneNumber.trim().replace(/-/g, "")}).populate('invoices').sort({date: -1}).exec((err, appointment) => {
				if (err){
					console.error(err);
					EmailService.sendAlert({error: err, info: 'Failed to retrieve appointment. Input data : ' + JSON.stringify(req.params)}, function(err, emailRes){

					});
					return res.status(500).send(err);
				}
				res.send(appointment);
			});			
		}
	});
};



