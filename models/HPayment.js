var keystone = require('keystone');
var Types = keystone.Field.Types;
const EmailService = require('../service/EmailService');

var storage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: 'documents/invoices'
    }
});

var Payment = new keystone.List('Payment', {
	nocreate: true,
	noedit: false,
	nodelete: true,
	sortable :true,
	defaultColumns: ['name', 'invoice', 'appointment', 'date', 'amount'],
	searchFields : ['invoice', 'appointment', 'date', 'name', 'amount']
});

Payment.add({
	date: { label: 'Payment Date', type: Types.Datetime, default: new Date(), noedit: true, required: true},
	appointment: { type: Types.Relationship, ref: 'Appointment', noedit: true},
	invoice: { type: Types.Relationship, ref: 'Invoice', noedit: true},
	name: {label: 'BrainTree Payment #', type: String, required: true, unique: true, noedit: true},
	amount: {type: Types.Money, required: true, noedit: true},
	successful: {type: Types.Boolean, default: false, noedit: true},
	receipt: {type: Types.Url,noedit: true},
	customerSignatureData: {type: String, noedit: true, hidden:true},
	customerSignature: {type: Types.Html, watch: true,  noedit: true, wysiwyg: true, value: function() {
			return '\<img src=\'' + this.customerSignatureData + '\'/\>';
		}
	},
	error: {type: String, noedit: true}
});

Payment.schema.post('save', function () {
	EmailService.sendPaymentAlert({payment: this}, (err, res)=>{

	});
});

Payment.defaultSort = '-date';

Payment.register();
