var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * User Model
 * ==========
 */
var User = new keystone.List('User');
var storage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: 'uploads',
        publicPath: '/public/uploads/',
    }
});

User.add({
	name: { type: Types.Name, required: true, index: true },
	email: { type: Types.Email, initial: true, required: true, unique: true, index: true },
	rate: { type: Types.Number, required: true, default: 100},
	zipCode: {type: Types.Number, required: false, index: true},
	image: { type: Types.CloudinaryImage, required: true, initial: true},
	workCategories: { type: Types.Relationship, ref: 'WorkCategory', required: false, many: true},
	phoneNumber: { type: Types.Text, index: true, required: true, initial: true},
	notes: {type: Types.Markdown, required: false},
	password: {type: Types.Password, initial: true, required: true },
	licenseFile: {type: Types.File, storage: storage}
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Administrator', index: true },
});

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function () {
	return this.isAdmin;
});

/**
 * Relationships
 */
//User.relationship({ ref: 'Post', path: 'posts', refPath: 'author' });
User.relationship({ ref: 'Appointment', path: 'appointments', refPath: 'assignedTo' });


/**
 * Registration
 */
User.defaultColumns = 'name, isAdmin, phoneNumber, workCategories, zipCode';
User.register();
