var keystone = require('keystone');
var Types = keystone.Field.Types;
const Config = require('./../service/Config');
const EmailService = require('./../service/EmailService');
	
/**
 * Appointment Model
 * =============
 */

var assignedToModified = false;
var cancelModified = false;

var Appointment = new keystone.List('Appointment', {
	//nodelete: true,
	sortable :true,
	defaultColumns: ['name', 'lastUpdate', 'date', 'customerName', 'fulfilled', 'cancel'],
	searchFields :['name', 'lastUpdate', 'customerInstructions', 'assignedTo', 'fulfilled', 'date', 'zipCode', 'phoneNumber', 'workCategories', 'addressLine1', 'addressLine2']
});

Appointment.add({
	createdAt: { label: 'Created At', type: Types.Datetime, default: new Date(), required: true, noedit: true},
	lastUpdate: { type: Types.Datetime, default: new Date(), noedit: true },
	date: { label: 'Schedule Date', type: Types.Date, required: true, index: true, initial:true},
	time: {type: Types.Text},
	slot: {type: Types.Number},
	name: { label: 'Appointment #', type: Types.Text, index: true, unique: true, initial: false, noedit: true},
	customerName: { label: 'Customer Name', type: Types.Name, required: true, initial: true},
	addressLine1: { type: Types.Text, label: 'Address', required: true, initial:true},
	addressLine2: { type: Types.Text},
	zipCode: { type: Types.Number},
	phoneNumber: { type: Types.Text, required: true, initial:true},
	email: { type: Types.Email},
	invoices: { type: Types.Relationship, ref: 'Invoice',  many: true},
	payment: { type: Types.Relationship, ref: 'Payment', noedit: true},
	workCategories: { type: Types.Relationship, ref: 'WorkCategory', many: true},
	customerInstructions: {type: Types.Text, noedit: true},
	companyNotes: {type: Types.Html, wysiwyg: true},
	assignedTo: { type: Types.Relationship, ref: 'User'},
	fulfilled: {type: Types.Boolean, default: false}, 
	cancel: {type: Types.Boolean, default: false}
});

Appointment.schema.pre('save', function (next) {
	assignedToModified = this.isModified('assignedTo');
	cancelModified = this.isModified('cancel');

	if (this.isNew) {
        Appointment.model.find().sort({name: -1 }).limit(1).exec((err, results) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'Failed to find last appointment to check last appointment id. '});
			}
			if (results && results.length){
				this.name = (1 + parseInt(results[0].name)).toString();
			}
			else {
				this.name = Config.appointmentIdStart.toString();
			}
			this.createdAt = new Date();
            next();
        });
    } else {
    	this.lastUpdate = new Date();
        next();
    }
});

Appointment.schema.post('save', function () {
	console.log('post save appointments');
	if (cancelModified) {
		cancelModified = false;
		if (this.cancel) {
			EmailService.cancelAppointment(this, this.assignedTo, function(err, res){
			});
		}	
		else {
			EmailService.sendAppointment(this, this.assignedTo, function(err, res){
			});
		}		
	}

	if (assignedToModified) {
		assignedToModified = false;
		if (!this.cancel) {
			EmailService.sendAppointment(this, this.assignedTo, function(err, res){
			});
		}
	}

});

Appointment.defaultSort = '-createdAt';

Appointment.register();
