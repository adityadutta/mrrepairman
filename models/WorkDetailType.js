var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * WorkDetailType Model
 * ==================
 */

var WorkDetailType = new keystone.List('WorkDetailType', {
	autokey: { from: 'name', path: 'key', unique: true },
});

WorkDetailType.add({
	name: { type: String, required: true },
});

WorkDetailType.relationship({ ref: 'WorkCategory', path: 'workCategories', refPath: 'WorkDetailType' });

WorkDetailType.register();
