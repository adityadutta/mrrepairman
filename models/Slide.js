var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Slide Model
 * =============
 */

var Slide = new keystone.List('Slide', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Slide.add({
	name: { label: 'Name', type: String, required: true },
	image: { label: 'Image', type: Types.CloudinaryImage }
});

Slide.register();
