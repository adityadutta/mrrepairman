var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Project Model
 * =============
 */

var Project = new keystone.List('Project', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Project.add({
	name: { type: String, required: true },
	description: { type: String },
	publishedDate: { type: Date, default: Date.now },
	mainImage: { type: Types.CloudinaryImage },
	images: { type: Types.CloudinaryImages },
	projectTypes: { type: Types.Relationship, ref: 'ProjectType', many: true }
});

Project.register();
