var keystone = require('keystone');
const Config = require('./../service/Config');
const crypto = require('./../service/crypto');
const Appointment = keystone.list('Appointment');
const Payment = keystone.list('Payment');
const ReferenceData = keystone.list('ReferenceData');
var Types = keystone.Field.Types;

var Invoice = new keystone.List('Invoice', {
	//nodelete: false,
	sortable :true,
	defaultColumns: ['name', 'lastUpdate', 'createdAt', 'appointment', 'payment', 'amount', 'closed'],
	searchFields : ['name', 'appointment', 'payment', 'amount', 'lastUpdate']
});

const getAmount = (amount) => {
	return amount ? amount : 0;
}

async function setCharges(invoice) {
	console.info('setcharges');
	let paid = 0;
	for (let i = 0 ; i < invoice.payments.length ; i++){
		let paymt = await Payment.model.findOne({_id: invoice.payments[i]}).exec();
		console.info('inside payment 1:' + paymt);
		if (paymt.successful){
			paid = paid + getAmount(paymt.amount);
			console.info('inside payment 2:' + paid);
		}
	}

	invoice.totalAmount =   getAmount(invoice.materialCost) + 
						    getAmount(invoice.laborCharge) + 
							getAmount(invoice.serviceCharge) + 
							getAmount(invoice.vanCharge) + 
							getAmount(invoice.disposalCharge)  - getAmount(invoice.discountAmount);

    invoice.paidAmount = getAmount(paid);
	invoice.balance = invoice.totalAmount - invoice.paidAmount;
	
	
	if (invoice.balance == 0){
		invoice.paid = true;
		invoice.closed = true;
		invoice.balance = 0;
	}
	
	console.info('return setcharges' + invoice.balance);
	return invoice;
}

ReferenceData.model.findOne({name: 'invoice-agreement-content'}).exec((err, agreement) => {
	if (err) {
		console.log(err)
	}
	else if (agreement) {
		Invoice.add({
			agreementContent: {type: Types.Html, wysiwyg: true, noedit: true, default: agreement.content}
		});
	}
});

Invoice.add({
	name: { label: 'Invoice#', type: Types.Text, index: true, noedit: true, unique: true, initial: false},
	lastUpdate: { type: Types.Datetime, default: new Date(), noedit: true },
	createdAt: { label: 'Created At', type: Types.Datetime, default: new Date(), required: true, noedit: true},
	encryptId: {type: Types.Text, noedit: true},
	appointment: { type: Types.Relationship, ref: 'Appointment', required: true, noedit: true, initial: true},
	payments: { type: Types.Relationship, ref: 'Payment', noedit: true, many: true},
	materialCost: {type: Types.Money},
	laborCharge: {type: Types.Money},
	serviceCharge: {type: Types.Money},
	vanCharge: {type: Types.Money},
	disposalCharge: {type: Types.Money},
	discountAmount: {type: Types.Money},	
	initialPayment: {type: Types.Money},
	totalAmount: {type: Types.Money, noedit: false},
	paidAmount: {type: Types.Money, noedit: false},
	balance: {type: Types.Money, noedit: true},
	closed: {type: Types.Boolean, default: false},
	paid: {type: Types.Boolean, default: false},
	agreementSignatureData: {type: String, noedit: true, hidden:true},
	agreementSignature: {type: Types.Html, watch: true,  noedit: true, wysiwyg: true, value: function() {
			return '\<img src=\'' + this.agreementSignatureData + '\'/\>';
		}
	},
	details: {type: Types.Html, wysiwyg: true, initial: true},
	materials: {type: Types.Html, wysiwyg: true, initial: true},
	sessions: {type: Types.Code, noedit: true, language: 'json'},
	companyNotes: {type: Types.Html, wysiwyg: true},
});

Invoice.schema.pre('save', function (next) {	
	
	if (this.isNew) {
		this.createdAt = new Date();
        Invoice.model.find().sort({name: -1 }).limit(1).exec((err, results) => {
			if (err){
				console.error(err);
				EmailService.sendAlert({error: err, info: 'Failed to find last invoice to check last appointment id. '});
			}
			if (results && results.length){
				this.name = (1 + parseInt(results[0].name)).toString();
			}
			else {
				this.name = Config.invoiceIdStart.toString();
			}
			this.encryptId = crypto.encrypt(this.name);
			
			Appointment.model.findOne({_id: this.appointment}).populate('payment').exec((err, appointment) => {
				if (err){
					console.error(err);
					EmailService.sendAlert({error: err, info: 'Failed to find appointment for invoice'});
				}
				else {
					if (appointment.invoices && (appointment.invoices.length > 0)) {
						this.serviceCharge = 0;
					}
					else {
						this.serviceCharge = Config.serviceFee;
					}
					if (appointment.payment){						
						this.payments = [appointment.payment._id];
						if (this.serviceCharge === Config.serviceFee){
							this.initialPayment = appointment.payment.amount;
						}			
					}

					setCharges(this).then((invoice) => {
						console.log('invoice recalculated ');
						this.totalAmount = invoice.totalAmount;
						this.balance = invoice.balance;
						this.closed = invoice.closed;
						this.paid = invoice.paid;
						this.paidAmount = invoice.paidAmount;
						this.discountAmount = invoice.discountAmount;
						next();
					});
								
				}				
			});	
		});
		
    } else {
		this.lastUpdate = new Date();
		setCharges(this).then((invoice)=>{
			console.info('returned setcharges' + invoice.balance);
			this.totalAmount = invoice.totalAmount;
			this.balance = invoice.balance;
			this.closed = invoice.closed;
			this.paid = invoice.paid;
			this.paidAmount = invoice.paidAmount;
			this.discountAmount = invoice.discountAmount;
			next();
		});
    }
});

Invoice.schema.post('save', function () {
	Appointment.model.findOne({_id: this.appointment}, (err, appointment) => {
		if (err){
			console.error(err);
		}
		else {
			if (!appointment.invoices){
				appointment.invoices = [];
			}
			if (appointment.invoices.indexOf(this._id) === -1){
				appointment.invoices = appointment.invoices.concat([this._id]);
				appointment.save();	
			}
		}
	});
});

Invoice.defaultSort = '-createdAt';

Invoice.register();


