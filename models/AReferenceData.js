var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ReferenceData Model
 * =============
 */

var ReferenceData = new keystone.List('ReferenceData', {
	sortable :true,
	defaultColumns: ['name', 'text', 'value', 'person', 'email'],
	searchFields : ['name', 'content', 'text', 'value', 'person', 'email']});

ReferenceData.add({
	name: { label: 'Name', type: Types.Text, required: true },
	image: { label: 'Image', type: Types.CloudinaryImage },
	content: {type:Types.Html, wysiwyg: true},
	date: {type:Types.Date},
	text : {type:Types.Text},
	value: {type:Types.Number},
	person: {type: Types.Name},
	email: {type: Types.Email}
});

ReferenceData.register();