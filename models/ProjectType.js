var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ProjectType Model
 * ==================
 */

var ProjectType = new keystone.List('ProjectType', {
	autokey: { from: 'name', path: 'key', unique: true },
});

ProjectType.add({
	name: { label: 'Name', type: String, required: true },
});

ProjectType.relationship({ ref: 'Project', path: 'projects', refPath: 'ProjectType' });

ProjectType.register();
