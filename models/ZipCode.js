var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ZipCode Model
 * =============
 */

var ZipCode = new keystone.List('ZipCode', {
	autokey: { from: 'name', path: 'key', unique: true },
	defaultColumns: ['description', 'name'],
	searchFields : ['description', 'name']
});

ZipCode.add({
	name: { label: 'Zip Code', type: Number, required: true },
	description: { type: String}
});

ZipCode.register();
