var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Testimonial Model
 * ==================
 */

var Testimonial = new keystone.List('Testimonial', {
	map: { name: 'comment' },
	autokey: { path: 'slug', from: 'comment', unique: true },
	sortable :true,
	defaultColumns: ['comment', 'sourceName', 'sourceLink'],
	searchFields : ['comment', 'sourceName', 'sourceLink']
});

Testimonial.add({
	comment: { label: 'Comment', type: String },
	commentor: { label: 'Commentator', type: String },
	linkLabel: {label: 'Link Label', type: String },
	linkSource: { label: 'Link Source', type: String},
	commentorImage: { label: 'Commentor Image', type: String}
});

Testimonial.register();
