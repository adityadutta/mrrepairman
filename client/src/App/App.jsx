import React from 'react';
import {
  HashRouter as Router,
  Route
} from 'react-router-dom';
import {PrivateRoute} from '../components';
import HomePage from '../HomePage';
import EmployeePage from '../EmployeePage';
import InvoicePage from '../InvoicePage';
import 'font-awesome/css/font-awesome.css';

export class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
          <Router>
              <Route exact path="/" component={HomePage} />
              <Route path="/employee" component={EmployeePage} />
              <Route path="/invoice/:id" component={InvoicePage} />
          </Router>
        );
    }
}
