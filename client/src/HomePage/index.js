import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import axios from 'axios';
import { systemConstants } from '../constants';
import {Image, Transformation} from 'cloudinary-react';
import repairmenImg from './repairmen.png';
import servicesImg from './services.svg';
import chicagoFlag from './chicago-flag.svg';
import chicagoImg from './chicago.svg';
import LaborImg from './labor.svg';
import ShoppingImg from './shopping.svg';
import PlugIcon from '../icons/plug';
import PlumbingIcon from '../icons/plumbing';
import DoorsIcon from '../icons/door-and-window';
import FloorIcon from '../icons/floor';
import BrickWallIcon from '../icons/brickwall';
import SawIcon from '../icons/saw';
import PaintingIcon from '../icons/painting';
import RoofIcon from '../icons/roof';
import ChatIcon from '../icons/chat';
import ScheduleIcon from '../icons/schedule';
import FanIcon from '../icons/fan';
import Logo from './../graphics/BigLogo.svg';
import bbbImg from './bbb.png';
import yelp5Img from './yelp5.png';
import yelpLogo from './Yelp_Logo.svg';
import lbiImg from './Licensed-Bonded-Insured.png';
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';
import NavItem from 'react-bootstrap/NavItem';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Grid from '@material-ui/core/Grid';
import Nav from 'react-bootstrap/Nav';
import { withStyles } from '@material-ui/core/styles';
import SchedulePage from '../SchedulePage';
import '../lib/Slicebox/css/slicebox.css';
import '../lib/Slicebox/css/custom.css';
import './../lib/Slicebox/js/jquery.slicebox';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import AppointmentPage  from './../AppointmentPage';
import TextField from '@material-ui/core/TextField';
import Scrollchor from 'react-scrollchor';
import './style.scss';
import Fade from 'react-reveal/Fade';

const HOST = systemConstants.SERVICE_URL ;


const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});


class HomePage extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        smallScreen: window.innerWidth < 768,
        appointmentModalOpen: false,
        searchInvoiceModalOpen: false,
        invoiceModalOpen: false,
        projectModalOpen: false,
        perHourRate: '',
        vanFee: '',
        serviceFee: '',
        email: null,
        phoneNumber: null,
        testimonials : [],
        projects: [],
        slides: [],
        workCategories: [],
        appointment: {},
        slidesShow : (<div></div>),
        testimonialsShow : (<div></div>),
        showIdx: 0,
        testimonialsSettings : {
          dots: true,
          infinite: true,
          pauseOnHover: false,
          speed: 1000,
          slidesToShow: 1,
          arrows: false,
          centerMode: false,
          autoplay: true,
          slidesToScroll: 1,
          autoplaySpeed: 4000
        }
      };
  }

  componentDidMount() {
    setTimeout(()=>{ 
      if (!(this.state.appointmentModalOpen || this.state.searchInvoiceModalOpen || this.state.invoiceModalOpen || this.state.appointmentModalOpen || this.state.projectModalOpen)) {
        this.chat();
      }
    }, 3000)
  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  componentWillMount() {
    axios.get(HOST + 'api/testimonials').then(res => {
      const testimonials = this.getTestimonialSlides(res.data);
      this.setState({testimonials: testimonials, 
        testimonialsShow: (<Slider {...this.state.testimonialsSettings}>
                            {testimonials}
                          </Slider>)
        });
    });
    axios.get(HOST + 'api/refdata/per-hour-rate').then(res => {
      this.setState({perHourRate: res.data[0].value});
    });
    axios.get(HOST + 'api/refdata/van-fee').then(res => {
      this.setState({vanFee: res.data[0].value});
    });
    axios.get(HOST + 'api/refdata/service-fee').then(res => {
      this.setState({serviceFee: res.data[0].value});
    });
  }

  getTestimonialSlides = (testimonials) => {
    if (!Array.isArray(testimonials)){
      return;
    }
    let items = [];
    if (testimonials.length > 0) {
        testimonials.forEach((testimonial) => {
        items.push(this.getTestimonialSlide(testimonial));
      });
    }
    return items;
  }

  searchInvoice = () => {
    axios.post(HOST + 'api/getLastAppointment', {phoneNumber: this.state.phoneNumber, email: this.state.email}).then(res => {
      this.setState({appointment: res.data, searchInvoiceModalOpen: false, invoiceModalOpen: true});
    });
  }

  getTestimonialSlide = (testimonial) => {
      return (
        <div className="owl-item cloned">
            <div>
              <div className="testimonial">
                  <figure className="mb-4 d-block align-items-center justify-content-center">
                    <div><img  src={testimonial.commentorImage} alt="Image" className="w-100 img-fluid mb-3 shadow"/></div>
                  </figure>
                  <blockquote className="mb-3">
                    <p>“{testimonial.comment}”</p>
                  </blockquote>
                  <p className="text-black"><strong>{testimonial.commentor}</strong></p>
              </div>
            </div>
        </div>
      );
  }

  getProjectSections = (projects) => {
    let sections = [];
    if (projects.length > 0) {
        projects.forEach((project, idx) => {
          sections.push(this.getProjectSection(project));
      });
    }
    return sections;
  }

  getSlides = (slides) => {
    let items = [];
    if (slides.length > 0) {
      slides.forEach((item, idx) => {
        items.push(this.getSlide(item));
      });
    }
    return items;
  }

  getSlide = (item) => {
    return (
      <li>
        <Image cloudName="djm3ipovr" version={item.image.version} format={item.image.format} publicId={item.image.public_id}>
        </Image>
      </li>
    );
  }

  getWorkCategories = (workCategories) => {
    let items = [];
    if (workCategories.length > 0) {
        workCategories.forEach((item, idx) => {
        items.push(this.getWorkCategory(item, idx));
      });
    }
    return items;
  }

  getWorkCategory = (item, idx) => {
    return ( 
    <Fade bottom delay={100*idx}>
        <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up" data-aos-delay="100">
          <div className="unit-4 d-block circle-img">
            <div className="unit-4-icon mb-3">
                <span className="icon-wrap">     
                <Image cloudName="djm3ipovr" version={item.image ? item.image.version : null} format={item.image ? item.image.format: null} publicId={item.image ? item.image.public_id : null}>
                    <Transformation width="200" height="200" crop="fill"/>
                </Image>
                </span>
            </div>
            <div>
                <h3>{item.name}</h3>
                <p>{item.description}</p>
            </div>
          </div>
        </div>
      </Fade>
    );
  }

  getProjectSection = (project) => {
    return (
      <div className="project">
        <div className="project-inner" onClick={() => {this.closeChat(); this.openProjectPage(project)}}>
          <Image cloudName="djm3ipovr" version={project.mainImage.version} format={project.mainImage.format} publicId={project.mainImage.public_id}>
            <Transformation height="300" width="300" crop="fill"/>
          </Image>
          <div className="project-content">
            <div className='header'>
            </div>
            <div className='project-details'>
              <div className="">{project.name}</div>
              <div className="">{project.description}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  chat = () => {
    var t = window.driftt = window.drift = window.driftt || [];
    if (!t.init) {
      if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
      t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
      t.factory = function(e) {
        return function() {
          var n = Array.prototype.slice.call(arguments);
          return n.unshift(e), t.push(n), t;
        };
      }, t.methods.forEach(function(e) {
        t[e] = t.factory(e);
      }), t.load = function(t) {
        var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
        o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
        var i = document.getElementsByTagName("script")[0];
        i.parentNode.insertBefore(o, i);
      };
    }
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('ngmyp4zihv2c');
  }

  openChat = () => {
    console.log('open chat...');
    if (window.drift && window.drift.api){
      window.drift.api.sidebar.open();
    }
    else {
      if (!this.chatInit){
        this.chat();
        this.chatInit = true;
      }
      setTimeout(() => {
        this.openChat();
      }, 500);      
    }
  }

  showChat = () => {
    console.log('open chat...');
    if (window.drift && window.drift.api){
       window.drift.api.widget.show();
    }
    else {
      this.chat();
    }
  }

  closeChat = () => {
    console.log('close chat...');
    if (window.drift && window.drift.api){
      window.drift.api.sidebar.close();
    }
  }

  closeScheduler = () => {
    this.setState({appointmentModalOpen: false});
    this.showChat();
  }

  closeSearchInvoice = () => {
    this.setState({searchInvoiceModalOpen: false});
    this.showChat();
  }

  closeInvoice = () => {
    this.setState({invoiceModalOpen: false});
    this.showChat();
  }

  openProjectPage = (project) => {
    this.setState({selectedProject: project, projectModalOpen: true});
  };

  openProjectClose = () => {
    this.setState({projectModalOpen: false});
  }

  openScheduler = () => {
    this.closeChat();
    this.setState({appointmentModalOpen: true});
  }

  openSearchInvoice = () => {
    this.closeChat();
    this.setState({searchInvoiceModalOpen: true});
  }

  openInvoice = () => {
    this.closeChat();
    this.setState({invoiceModalOpen: true});
  }

  sliceboxReady = () => {
   let slicebox = $( '#sb-slider' );
   if (slicebox && !this.slicebox) {
      this.slicebox = slicebox;
      this.slicebox = this.slicebox.slicebox( {
        onReady : () => {
          //this.slicebox.play();
        },
        orientation : 'r',
        autoplay : true,
        colorHiddenSides : 'rgba(0,0,0,0)',
        disperseFactor : 0,
        cuboidsRandom : true,
        perspective : 1200,
        interval: 4000,
      });
    }
  }

  componentDidUpdate() {
    this.sliceboxReady();
  }

  render() {

    return (
      <div className='wrapper'>
        <Dialog fullScreen open={this.state.appointmentModalOpen} onClose={this.closeScheduler} aria-labelledby="responsive-dialog-title" fullWidth={true}>
            <DialogTitle onClose={this.closeScheduler} id="responsive-dialog-title">Schedule a Repairman</DialogTitle>
            <DialogContent>
                <SchedulePage></SchedulePage>
            </DialogContent>
        </Dialog>

        <Dialog fullScreen={this.props.fullScreen} open={this.state.searchInvoiceModalOpen} onClose={this.closeSearchInvoice} fullWidth={true}>
            <DialogTitle onClose={this.closeSearchInvoice}>Check Your Invoice</DialogTitle>
            <DialogContent>
                <Grid container spacing={2} className="invoicelookup">
                    <Grid item sm={12}>
                        <TextField fullWidth autoFocus margin="dense" id="name" value={this.state.phoneNumber} onChange={(e)=> {this.setState({phoneNumber: e.target.value})}} label="Phone Number" />
                    </Grid>
                    <Grid item sm={12}>
                        <TextField fullWidth autoFocus margin="dense" type="email" id="email" value={this.state.email} onChange={(e)=> {this.setState({email: e.target.value})}} label="Email" />
                    </Grid>
                </Grid>

            </DialogContent>
            <DialogActions>
                <Button onClick={this.searchInvoice} color="primary">
                    Lookup
                </Button>
            </DialogActions>
        </Dialog>

        <Dialog fullScreen={this.props.fullScreen} open={this.state.invoiceModalOpen} onClose={this.closeInvoice} fullWidth={true}>
            <DialogTitle onClose={this.closeInvoicePay}></DialogTitle>
            <DialogContent>
                <AppointmentPage readOnly={true} appointment={this.state.appointment}></AppointmentPage>
            </DialogContent>
        </Dialog>
        <Navbar collapseOnSelect expand="lg" bg="light" variant="light" fixed="top" className='mynavbar'>
            <Navbar.Brand href="/"><img src={Logo} className='logo'></img>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <NavItem>
                        <Scrollchor to="#sec-services" className="nav-link easy">Services</Scrollchor>
                    </NavItem>
                    <NavItem>
                        <Scrollchor to="#sec-pricing" className="nav-link easy">Pricing</Scrollchor>
                    </NavItem>
                    <NavItem>
                        <Scrollchor to="#sec-testimonials" className="nav-link easy">Testimonials</Scrollchor>
                    </NavItem>
                    <NavDropdown title="Customers" id="collasible-nav-dropdown">
                        <NavDropdown.Item onClick={this.openSearchInvoice} className="nav-link easy">Invoice</NavDropdown.Item>
                    </NavDropdown>

                    <NavDropdown title="Jobs" id="collasible-nav-dropdown">
                        <NavDropdown.Item target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSciELahpfjp2f0ilyNxwscUHl9kOo7ixgZwMPsN7cw2khASsA/viewform">Apply for Technician</NavDropdown.Item>
                    </NavDropdown>

                </Nav>
                <Nav>
                    <a href="https://www.yelp.com/biz/mr-repairman-chicago" target='_blank'><img src={yelpLogo} className="yelp-header"></img></a>
                    <Nav.Link>
                        <button onClick={this.openScheduler} className="btn btn-primary mr-2 mb-2"> <ScheduleIcon className="btn-icon"/> Schedule a Repairman</button>
                    </Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
        
        <div className="site-blocks-cover">
            <div className="container">
                <div className="row align-items-center justify-content-center">
                    <div className="col-md-12 rel">
                        <img src={repairmenImg} alt="Image" className="img-fluid img-absolute"></img>
                        <div className="row mb-4">
                            <div className="col-lg-4 mr-auto">
                                <h1>Repairs Made Easy</h1>
                                <h5 className="mb-5">Finally a team that understands your time is valuable and gets things done right.</h5>
                                <div>                                    
                                  <button onClick={this.openScheduler} className="btn btn-primary mr-2 mb-2"> <ScheduleIcon className="btn-icon"/> Schedule a Repairman</button>
                                </div>
                                <div>
                                    <button onClick={this.openChat} className="btn btn-primary btn-lg"> <ChatIcon className="btn-icon"/> Talk to Us</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="container">
          <div className="row mb-5 site-section">
              <Fade left>
                  <div className="col-lg-7 aos-init" data-aos="fade-right">
                      <img src={servicesImg} alt="Image" className="img-fluid" />
                  </div>
              </Fade>
              <div className="col-lg-5 pl-lg-5 ml-auto mt-md-5 text-center">
                  <h2 className="text-black section-title">No repair too small</h2>
                  <h5 className="mb-4">We have team with versatile skills that will help you crush your repair list.</h5>
                  <Fade right>
                      <div className="author-box aos-init" data-aos="fade-left">
                          <div className="d-flex mb-4">
                              <div className="mr-3">
                                  <img alt="Image" src="https://s3-media0.fl.yelpcdn.com/photo/MC2Fbjdjo52O9vCTDiKEFQ/120s.jpg" className="img-fluid rounded-circle" />
                              </div>
                              <div className="mr-auto text-black">
                                  <strong className="font-weight-bold mb-0">Elizabeth L.</strong>
                                  <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lakeview, Chicago, IL
                              </div>
                          </div>
                          <blockquote>“Mr. Repairman was fantastic! On the day of the fix, David came early. He was honest and efficient. He had everything he needed to fix the heat with him. He explained everything and double-checked all connections before he finished.”</blockquote>
                      </div>
                  </Fade>
              </div>
          </div>

          <div className="site-section" id="sec-services">
              <div className="container">
                  <Fade bottom>
                      <div className="row mb-5 justify-content-center text-center aos-init" data-aos="fade-up">
                          <div className="col-7 text-center  mb-5">
                              <h2 className="section-title">We Will Repair</h2>
                          </div>
                      </div>
                  </Fade>
                  <div className="row align-items-stretch">
                      <Fade bottom>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><PlugIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Electrical</h5>
                                      <p>Electrical services include fixture installations and troubleshooting.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade bottom delay={100}>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up" data-aos-delay="100">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><PlumbingIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Plumbing</h5>
                                      <p>Toilet repair, leaks, clogs, fixtures, garbage disposals, hook ups.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade bottom delay={200}>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up" data-aos-delay="200">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><DoorsIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Doors & Windows</h5>
                                      <p>Installation & repair of a variety of windows and doors. Commercial as well as ADA doors.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade bottom>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><PaintingIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Painting & Wall Repair</h5>
                                      <p>We color match interior or exterior spaces. Apoxy painting concrete surfaces.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade bottom delay={100}>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up" data-aos-delay="100">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><FloorIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Tile & Flooring</h5>
                                      <p>Various custom flooring applications including wood, ceramic, stone and heated floor systems.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade bottom delay={200}>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up" data-aos-delay="200">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><RoofIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Roofing</h5>
                                      <p>Repair or new installations of pitched, flat, Gable, Mansard, Gambrel, Turrets and hip roofs.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade bottom>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><FanIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>HVAC</h5>
                                      <p>We install and repair HV/AC systems of various brands. Installation of duct work and ventilation systems.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade bottom delay={100}>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up" data-aos-delay="100">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><BrickWallIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Tuckpointing</h5>
                                      <p>Complete masonry restorations including tuck pointing, brick replacement. New builds.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade bottom delay={200}>
                          <div className="col-md-6 col-lg-4 mb-4 mb-lg-4 aos-init" data-aos="fade-up" data-aos-delay="200">
                              <div className="unit-4 d-block">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap"><span className="text-primary"><SawIcon className="service-icon"/></span></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Carpentry</h5>
                                      <p>Installation of wood and various synthetic materials. Crown moulding and baseboard trims.</p>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                  </div>
              </div>
          </div>

          <div className="site-section" id="sec-pricing">
              <div className="container">
                  <Fade bottom>
                      <div className="row mb-5 justify-content-center text-center aos-init" data-aos="fade-up">
                          <div className="col-7 text-center  mb-5">
                              <h2 className="section-title">Simple Fair Rates</h2>
                              <h5 className="mb-4">Schedule an appointment for ${this.state.serviceFee}. Minimum duration is 2 hours charged at hourly rates and invoiced at job completion.</h5>
                          </div>
                      </div>
                  </Fade>
                  <div className="row align-items-stretch">
                      <Fade left>
                          <div className="col-md-6 col-lg-6 mb-4 mb-lg-4 aos-init" data-aos="fade-up">
                              <div className="unit-4 d-block big-circle-img">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap remove-background"><img src={LaborImg} alt="Image" className="img-fluid pricingimg"/></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Labor</h5>
                                      <h5 className='bolder'>${this.state.perHourRate}/hr</h5>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                      <Fade right>
                          <div className="col-md-6 col-lg-6 mb-4 mb-lg-4 aos-init" data-aos="fade-up" data-aos-delay="100">
                              <div className="unit-4 d-block big-circle-img">
                                  <div className="unit-4-icon mb-3">
                                      <span className="icon-wrap remove-background"><img src={ShoppingImg} alt="Image" className="img-fluid pricingimg"/></span>
                                  </div>
                                  <div>
                                      <h5 className='bolder'>Van Fee</h5>
                                      <h5 className='bolder'>${this.state.vanFee}</h5>
                                  </div>
                              </div>
                          </div>
                      </Fade>
                  </div>       
              </div>
              <div className="col-lg-12 ml-auto pl-lg-5 text-center">
                <button onClick={this.openScheduler} className="btn btn-primary mr-2 mb-2"><ScheduleIcon className="btn-icon"/> Schedule a Repairman</button>
              </div>
          </div>
        </div>

        <div className="site-section bg-light" id="sec-about">
          <div className="container">
              <div className="row mb-5">
                  <div className="col-12 text-center">
                      <h2 className="section-title mb-3">About Us</h2>
                  </div>
              </div>
              <div className="row mb-5">
                  <Fade left>
                      <div className="col-lg-6 aos-init" data-aos="fade-right">
                          <img src={chicagoImg} alt="Image" className="img-fluid skyline" />
                      </div>
                  </Fade>
                  <div className="col-lg-5 ml-auto pl-lg-5 text-center">
                      <h5 className="mb-4"><span className='textLogo'><span className='red'>MR</span><span className='blue'>REPAIRMAN</span></span> is your source for professional, experienced and local home repairs of all types. Trusted by homeowners and business owners in Chicagoland since 2005.</h5>
                      <button onClick={this.openChat} className="btn btn-primary btn-lg"><ChatIcon className="btn-icon"/> Talk to Us</button>
                  </div>
              </div>
          </div>
      </div>

      <div className="site-section" id="our-team-section">
        <div className="container">
          <div className="row mb-5 justify-content-center text-center aos-init" data-aos="fade-up">
            <div className="col-10 text-center  mb-5">
                <h2 className="section-title">Accreditations and Social Media</h2>
            </div>
          </div>
          <div className='row'>
            <Grid container>
              <Grid item sm={12} md={4}>
                <Fade bottom>
                  <div className="text-center acrredition-card">
                    <img src={bbbImg} id="bbbImg" />
                  </div>
                </Fade>
              </Grid>
              <Grid item sm={12} md={4}>
                <Fade bottom delay={100}>
                  <div className="text-center acrredition-card">
                      <img src={lbiImg} id="lbiImg" />
                  </div>
                </Fade>
              </Grid>
              <Grid item sm={12} md={4}>
                <Fade bottom delay={200}>
                  <div className="text-center acrredition-card">
                    <a target='_blank' href='https://www.yelp.com/biz/mr-repairman-chicago'><img src={yelp5Img} id="yelpImg"/></a>
                  </div>
                </Fade>
              </Grid>    
            </Grid>
          </div>

        </div>
      </div>
        
      <div className="site-section testimonial-wrap bg-light" id="sec-testimonials">
          <div className="container">
              <div className="row mb-5">
                  <div className="col-12 text-center">
                      <h2 className="section-title mb-3">Testimonials</h2>
                  </div>
              </div>
          </div>
          {this.state.testimonialsShow}
      </div>

      <footer className="footer-distributed">
          <div className="footer-left">
              <img className="logo" src={Logo}/>
              <p className="footer-links">
                  <Scrollchor to="#" className="footer-link">Home</Scrollchor>
                  <Scrollchor to="#sec-pricing" className="footer-link">Rates</Scrollchor>
                  <Scrollchor to="#sec-about" className="footer-link">About</Scrollchor>
                  <a href='javascript:void(0)' onClick={this.openChat} className="footer-link">Contact</a>
              </p>
              <div className="employeeLogin">
                  <Link to="/employee">Employee Login</Link>
              </div>

              <div className="footer-company-name">MrRepairman &copy; 2020</div>
  
          </div>
          <div className="footer-center">
              <div>
                  <i className="fa fa-map-marker"></i>
                  <p><span>3023 N Clark St, Suite 292</span> Chicago, IL 60657</p>
              </div>
              <div>
                  <i className="fa fa-phone"></i>
                  <p>773.425.1410</p>
              </div>
              <div>
                  <i className="fa fa-envelope"></i>
                  <p><a href="mailto:contact@mrrepairman.com">contact@mrrepairman.com</a></p>
              </div>
          </div>
          <div className="footer-right">
              
              <div className="footer-company-about">
                  <span>About the company</span>
                  <a href='https://www.mrrepairman.com'>mrrepairman</a> is your source for professional, experienced and local home repairs of all types. Trusted by homeowners and business owners in Chicagoland since 2005.
              </div>

              <div className="chicago">
                  Built in Chicago
                  <img src={chicagoFlag} className='flag'></img>
              </div>
          </div>
      </footer>
    </div>
    );
  }
}

HomePage.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

export default withMobileDialog()(HomePage);
