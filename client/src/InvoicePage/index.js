import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { systemConstants } from '../constants';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import DropIn from "braintree-web-drop-in-react";
import Logo from './../graphics/BigLogo.svg';
import SignatureCanvas from 'react-signature-canvas';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Container from '@material-ui/core/Container';
import MaterialTable, { MTableToolbar }from 'material-table';
import TodayIcon from '@material-ui/icons/Today';
import AssignmentIcon from '@material-ui/icons/AssignmentOutlined';
import CloseIcon from '@material-ui/icons/Close';
import DescriptionIcon from '@material-ui/icons/DescriptionOutlined';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCartOutlined';
import PersonOutlineIcon from '@material-ui/icons/PersonOutlineOutlined';
import Snackbar from '@material-ui/core/Snackbar';
import PaymentIcon from '@material-ui/icons/PaymentOutlined';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import './style.scss';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';

const HOST = systemConstants.SERVICE_URL ;
const styles = {
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  root: {
    width: '100%',
  },
  paper: {
    padding: '10px',
    margin: '10px',
    textAlign: 'center',
    color: 'rgb(100,100,100)',
  },
  container : {
    padding:'20px'
  },
  textField: {
    marginLeft: '5px',
    marginRight: '5px',
    width: '100%',
  },
};

const getAmount = (amount) => {
  return amount ? amount : 0;
}

class InvoicePage extends React.Component {
 	constructor(props) {
      super(props);
      this.readOnly = props.readOnly;
      this.brainTree;
      this.textToolbarOptions = {
        options: ['inline', 'blockType', 'list', 'textAlign', 'history'],
        inline: { 
          inDropdown: true, 
          options: ['bold', 'italic', 'monospace']
        },
        blockType: {
          inDropdown: true,
          options: ['normal']
        },
        fontSize: 9,
        fontFamily: {
          options: ['Arial']
        },
        list: { inDropdown: true },
        textAlign: { inDropdown: true },
        link: { inDropdown: true },
        history: { inDropdown: true },
      };
      this.sessionColumns =  [
        { title: 'Date', field: 'date', type: 'date' },
        { title: 'Start Time', field: 'startTime', type: 'time' },
        { title: 'End Time', field: 'endTime', type: 'time' },
        { title: 'Rate (hr)', field: 'rate', type: 'currency', editable: 'never', currencySetting : {}},          
        { title: 'Total Hours', field: 'hours', type: 'numeric', editable: 'never'},
        { title: 'Amount', field: 'amount', type: 'currency', editable: 'never' , currencySetting : {}},
      ];
      this.costTableColumns = [
        { title: '', field: 'name', editable: 'never' },
        { title: '', field: 'amount', type: 'currency', editComponent: props => (
          <input
            type="numeric"
            value={props.value}
            onChange={e => props.onChange(e.target.value)}
          />
          ) 
        }
      ];
	    this.state = {
        invoice: null,
        agreementSignatureCanvas: null, 
        customerSignatureCanvas: null,
        details: EditorState.createEmpty(),
        materials: EditorState.createEmpty(), 
        agreementContent: EditorState.createEmpty(),
        saved: false,
        loading: true,
        paid: false,
        amount: 0,
        materialCost: 0,
        shoppingCost: 0,
        enablePayment: false,
        sessions: [],
        costs : {
          materialCost: {name: 'Materials', amount: 0, factor: 1, field: 'materialCost'},
          laborCharge: {name: 'Total Labor', amount: 0, factor: 1, field: 'laborCharge'},
          serviceCharge: {name: 'Service Charge', amount: 0, factor: 1, field: 'serviceCharge'},
          vanCharge: {name: 'Van Charge', amount: 0, factor: 1, field: 'vanCharge'},
          disposalCharge: {name: 'Disposal Fees', amount: 0, factor: 1, field: 'disposalCharge'},
          discountAmount: {name: 'Promotion Discount', amount: 0, factor: -1, field: 'discountAmount'},
          totalAmount: {name: 'Total', amount: 0, factor: 0, field: 'totalAmount'},
          paidAmount: {name: 'Paid To Date', amount: 0, factor: -1, field: 'paidAmount'},
          balance: {name: 'BALANCE', amount: 0, factor: 0, field: 'balance'}
        }
      };

      let endpoint = null;
      if (props.match && props.match.params && props.match.params.id){  
        this.readOnly = true;  
        endpoint = 'api/invoiceid/' + props.match.params.id;
      }
      else if (!this.readOnly) {
        props.saveCbk(this.saveInvoice);
        endpoint = 'api/invoice/' + props.invoice.name;
      }
      else {
        endpoint = 'api/invoiceid/' + props.invoice._id;
      }

      axios.get(HOST + endpoint).then(res => {
        if (res.data === 401){
          window.location.href = '/login';
        }
        else {
          let invoice = res.data;
          this.setInvoiceState(invoice);

          //if (!invoice.closed && !this.readOnly) {
            axios.get(HOST + 'api/users').then(res => {
              if (res.data === 401){
                window.location.href = '/login';
              }
              let userLookup = {};
              let userMap = {};
              res.data.users.forEach((user, index) => {
                  userLookup[user.email] = user.name.first + ' ' + user.name.last;
                  userMap[user.email] = user;
               });
              let cols = this.sessionColumns;
              cols.splice(1, 0, { title: 'Technician', field: 'user', lookup: userLookup});
              this.setState({userMap: userMap, users: res.data.users, userLookup: userLookup, columns: cols});
            
            })
            .catch(err => {
              console.log(err)
              return this.setState({ confirmationSnackbarMessage: "Employee loading failed", confirmationSnackbarOpen: true })
            });
          //}
          if (!invoice.closed) {
            axios.get(HOST + 'api/client_token').then(res => {
              this.state.clientToken = res.data;
              this.setState({clientToken: res.data, loading: false});
              console.log('received client token');
            })
            .catch(err => {
              console.log(err)
              return this.setState({ confirmationSnackbarMessage: "Invoice loading failed", confirmationSnackbarOpen: true, loading: false })
            });
          }
          this.setState({loading: false});
        }
      })
      .catch(err => {
        console.log(err)
        return this.setState({ confirmationSnackbarMessage: "Invoice loading failed", confirmationSnackbarOpen: true })
      });
	}

  handlePay = () => {
    this.brainTree.requestPaymentMethod((requestPaymentMethodErr, payload) => {
      axios.post(HOST + 'api/checkout/invoice', 
        {
          amount: this.state.amount, 
          paymentMethodNonce: payload.nonce,
          invoice : this.getInvoiceState()
       })
      .then(response => {
        this.brainTree.teardown((teardownErr) => {
          if (teardownErr) {
            console.error('Could not tear down Drop-in UI!');
          } else {
            console.info('Drop-in UI has been torn down!');
          }
        });        
        this.setState({paid: true, amount: 0, confirmationSnackbarMessage: "Payment succesfull", confirmationSnackbarOpen: true, processed: true });
        this.setInvoiceState(response.data);
      })
      .catch(err => {
        console.log(err)
        return this.setState({ confirmationSnackbarMessage: "Payment failed", confirmationSnackbarOpen: true })
      })
    });
  }

  onWorkRecordAdd = (workItem) => {
    return new Promise(resolve => {
      resolve();
      const sessions = [...this.state.sessions];
      let ratedWorkItem = this.getSessionItemRated(workItem);
      sessions.push(ratedWorkItem);
      let laborCharge = this.calculateTotalLabor(sessions);
      this.state.costs.laborCharge.amount = laborCharge;
      this.state.sessions = sessions;
      this.setState({sessions: sessions, costs: this.state.costs});
      this.saveInvoice();
    });
  }

  onWorkRecordUpdate = (newData, oldData) => {
    return new Promise(resolve => {
      resolve();
      const sessions = [...this.state.sessions];
      if (typeof newData.date === 'string') {
        newData.date = new Date(newData.date);
      }
      if (typeof newData.startTime === 'string') {
        newData.startTime = new Date(newData.startTime);
      }
      if (typeof newData.endTime === 'string') {
        newData.endTime = new Date(newData.endTime);
      }
      let ratedData = this.getSessionItemRated(newData);
      sessions[sessions.indexOf(oldData)] = ratedData;
      let laborCharge = this.calculateTotalLabor(sessions);
      this.state.costs.laborCharge.amount = laborCharge;
      this.state.sessions = sessions;
      this.setState({sessions: sessions, costs: this.state.costs});
      this.saveInvoice();
    });
  }

  getCostList = (items) => {
    return Object.keys(items).map((key, i) => {
      return items[key];
    });
  }

  onChargeUpdate = (newData, oldData) => {
    return new Promise(resolve => {
      resolve();
      this.state.costs[newData.field] = newData;
      this.setState({costs: this.state.costs});
      this.saveInvoice();
    });
  }


  getSessionItemRated = (data) => {
    data.rate = this.getRate(data);
    if (data.endTime) {
      data.endTime.setSeconds(0);
      data.endTime.setMilliseconds(0);
    } 
    if (data.startTime) {
      data.startTime.setSeconds(0);
      data.startTime.setMilliseconds(0);
    }
    data.hours = this.getTotalHours(data);    
    data.amount = data.hours * data.rate;  
    return data
  }

  calculateTotalLabor = (sessions) => {
    let total = 0;
    sessions.forEach((item, index) => {
      total = total + parseFloat(item.amount) ;
    });
    return total;
  }

  getTotalHours = (rec) => {
    let hours = 0;
    if (rec.endTime && rec.startTime) {
      if (rec.endTime.getTime() > rec.startTime.getTime()) {
        let intervals = Math.ceil((rec.endTime.getTime() - rec.startTime.getTime())/(1000*60*15));
        hours = intervals/4;            
      }
    }
    return hours;
  }

  getRate = rec => {
    let rate = 100;
    if (rec.user) {
       rate = this.state.userMap[rec.user].rate;
    }
    return rate;
  }

  handleCustomerSignatureRef = (ref) => {
    this.state.customerSignatureCanvas = ref;
    this.setInvoiceState(this.state.invoice);
  }

  handleAgreementSignatureRef = (ref) => {
    this.state.agreementSignatureCanvas = ref;
    this.setInvoiceState(this.state.invoice);
  }

  clearCustomerSignature = () => {
    this.state.customerSignatureCanvas.clear();
  }

  clearAgreementSignature = () => {
    this.state.agreementSignatureCanvas.clear();
  }

  onDetailsChange = (editorState) => {
     this.setState({
      details : editorState
    });
  }

  onMaterialsChange = (editorState) => {
     this.setState({
      materials : editorState
    });
  }

  getEditorStateFromMarkup = (markup) => {
    if (!markup) {
      return EditorState.createEmpty();
    }
    const blocksFromHtml = htmlToDraft(markup);
    const { contentBlocks, entityMap } = blocksFromHtml;
    const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
    return  EditorState.createWithContent(contentState);
  }

  getInvoiceState = () => {
    let invoice = this.state.invoice;
    return {
      name : invoice.name,
      materialCost: this.state.costs.materialCost.amount,
      laborCharge:this.state.costs.laborCharge.amount,
      serviceCharge:this.state.costs.serviceCharge.amount,
      vanCharge:this.state.costs.vanCharge.amount,
      disposalCharge:this.state.costs.disposalCharge.amount,
      discountAmount:this.state.costs.discountAmount.amount,
      totalAmount: this.state.costs.totalAmount,
      balance: this.state.costs.balance.amount,
      sessions : JSON.stringify(this.state.sessions),
      materials: this.getMarkupFromEditorState(this.state.materials),
      details: this.getMarkupFromEditorState(this.state.details),
      agreementContent: invoice.agreementContent,
      customerSignatureData: this.state.customerSignatureCanvas.toDataURL('image/png'),
      agreementSignatureData: this.state.agreementSignatureCanvas.toDataURL('image/png')
    };
  }

  getMarkupFromEditorState = (editorState) => {
    return draftToHtml(
      convertToRaw(editorState.getCurrentContent()));
  }

  setInvoiceState = (invoice) => {
    if (this.state.agreementSignatureCanvas) {
      this.state.agreementSignatureCanvas.clear();
      this.state.agreementSignatureCanvas.fromDataURL(invoice.agreementSignatureData, {width:450, height:200});      
    }

    let sessions = this.state.sessions;
    if (invoice.sessions) {
      sessions = JSON.parse(invoice.sessions);
    }
 
    sessions.forEach((session, i) => {
      session.startTime = new Date(session.startTime);
      session.endTime = new Date(session.endTime);
      session.date = new Date(session.date);
    });

    if (this.readOnly || invoice.closed || invoice.agreementSignatureData ){
      if (this.state.agreementSignatureCanvas){
        this.state.agreementSignatureCanvas.off();        
      }
    }
    let total = getAmount(invoice.materialCost) 
                + getAmount(invoice.laborCharge) 
                + getAmount(invoice.serviceCharge) 
                + getAmount(invoice.vanCharge)
                + getAmount(invoice.disposalCharge) - getAmount(invoice.discountAmount);

    this.state.costs.materialCost.amount = getAmount(invoice.materialCost);
    this.state.costs.laborCharge.amount = getAmount(invoice.laborCharge);
    this.state.costs.serviceCharge.amount = getAmount(invoice.serviceCharge);
    this.state.costs.vanCharge.amount = getAmount(invoice.vanCharge);
    this.state.costs.disposalCharge.amount = getAmount(invoice.disposalCharge);
    this.state.costs.discountAmount.amount = getAmount(invoice.discountAmount);
    this.state.costs.totalAmount.amount = total;
    this.state.costs.balance.amount = getAmount(invoice.balance);
    this.state.costs.paidAmount.amount = getAmount(invoice.paidAmount);
    
    this.setState({
      invoice: invoice,
      balance : this.state.costs.balance.amount,
      amount : this.state.costs.balance.amount,
      costs: this.state.costs,
      sessions: sessions,
      agreementSignatureData: invoice.agreementSignatureData,
      details: this.getEditorStateFromMarkup(invoice.details),
      materials: this.getEditorStateFromMarkup(invoice.materials),
      agreementContent: this.getEditorStateFromMarkup(invoice.agreementContent)
    });
  }

  saveAgreement = () => {
      let invoice = this.getInvoiceState();
      axios.post(HOST + 'api/invoice/agreement', invoice)
      .then(response => {
        this.setState({
          confirmationSnackbarMessage: "Agreement signed succesfully", 
          confirmationSnackbarOpen: true, 
          processed: true });
        this.setInvoiceState(response.data);
      })
      .catch(err => {
        console.log(err);
        return this.setState({ confirmationSnackbarMessage: "Agreement sign failed", confirmationSnackbarOpen: true })
      })
  }

  saveSessions = () => {
      let invoice = this.getInvoiceState();
      axios.post(HOST + 'api/invoice/sessions', invoice)
      .then(response => {
        this.setState({
          confirmationSnackbarMessage: "Sessions saved succesfully", 
          confirmationSnackbarOpen: true, 
          processed: true });
        this.setInvoiceState(response.data);
      })
      .catch(err => {
        console.log(err);
        return this.setState({ confirmationSnackbarMessage: "Session save failed", confirmationSnackbarOpen: true })
      })
  }

  saveDetails = () => {
      let invoice = this.getInvoiceState();
      axios.post(HOST + 'api/invoice/details', invoice)
      .then(response => {
        this.setState({
          confirmationSnackbarMessage: "Details saved succesfully", 
          confirmationSnackbarOpen: true, 
          processed: true });
        this.setInvoiceState(response.data);
      })
      .catch(err => {
        console.log(err);
        return this.setState({ confirmationSnackbarMessage: "Details save failed", confirmationSnackbarOpen: true })
      })
  }

  saveMaterials = () => {
      let invoice = this.getInvoiceState();
      axios.post(HOST + 'api/invoice/materials', invoice)
      .then(response => {
        this.setState({ 
          confirmationSnackbarMessage: "Materials saved succesfully", 
          confirmationSnackbarOpen: true, 
          processed: true });
        this.setInvoiceState(response.data);
      })
      .catch(err => {
        console.log(err);
        return this.setState({ confirmationSnackbarMessage: "Materials save failed", confirmationSnackbarOpen: true })
      })
  }

  saveInvoice = () => {
    let invoice = this.getInvoiceState();
    axios.post(HOST + 'api/invoice', invoice)
    .then(response => {
      this.setState({ 
        confirmationSnackbarMessage: "Invoice saved succesfully", 
        confirmationSnackbarOpen: true, 
        processed: true });
      this.setInvoiceState(response.data);
    })
    .catch(err => {
      console.log(err);
      return this.setState({ confirmationSnackbarMessage: "Invoice save failed", confirmationSnackbarOpen: true })
    })
  }

  render() {
    let invoice = this.state.invoice;
    if (!invoice) {
      return null;
    }
    
    let payment = (<div></div>);

    if (invoice.paid) {
      payment = (
      <div className='container'>
        <p></p>
        <div className='conf'>Invoice fully paid in the amount of 
          <span className='bold'> ${invoice.paidAmount}</span>.
        </div>
      </div>
      );
    }
    else if (invoice.closed){
      payment = (<div className='conf'>This invoice is closed.</div>);
    }
    else if (this.state.clientToken){
    payment = (
      <Grid container md={12} spacing={3}>
        <Grid container item sm={12} md={12}>
          <Grid item md={12}>
            <h6>Customer Signature</h6>
          </Grid>
          <Grid item md={12}>
            <SignatureCanvas ref={this.handleCustomerSignatureRef} penColor='black'
              canvasProps={{className: 'sigCanvas'}} />
          </Grid>
          <Grid item md={12}>
            <Button onClick={this.clearCustomerSignature} >Clear</Button>
          </Grid>
        </Grid>
        <Grid container item sm={12} md={12}>
          <Grid container item md={12}>
            <DropIn
              options={{ authorization: this.state.clientToken }}
              onInstance={instance => (this.brainTree = instance)}/>
          </Grid>
          <Grid container item md={12} spacing={1}>
            <Grid item md={4}>
              <FormControl fullWidth variant="outlined">
                <InputLabel htmlFor="outlined-adornment-amount">Balance</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-balance"
                  disabled
                  value={this.state.balance}
                  startAdornment={<InputAdornment position="start">$</InputAdornment>}
                  labelWidth={60}
                />
              </FormControl>
            </Grid>
          <Grid item md={4}>
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="outlined-adornment-amount">Payment</InputLabel>
              <OutlinedInput
                id="outlined-adornment-amount"
                value={this.state.amount}
                onChange={(event) => {this.setState({amount: event.target.value})}}
                startAdornment={<InputAdornment position="start">$</InputAdornment>}
                labelWidth={60}
              />
            </FormControl>
          </Grid>
          <Grid item md={4}>
            <Button
              disabled={this.state.paid}
              variant="contained" color="primary"
              onClick={() => this.handlePay()} >
              Pay
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>);
    }

    const classes = this.props.classes;

    return (
      <Container maxWidth="lg">
        <Grid container spacing={4}>
          <Grid item sm={12} md={12}></Grid>
          
          <Grid item sm={12} md={12}>
            <Paper className='container' square={true}>
              <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}> 
                    <img src={Logo} className='logo'></img>
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <p><span>3023 N Clark St, Suite 292</span> Chicago, IL 60657</p>
                  </Grid>
              </Grid>            
            </Paper>
          </Grid>
    
          <Grid item sm={12} md={12}>
            <Paper className='container' square={true}>
              <div className='invoice-section-header'>
                <PersonOutlineIcon className='icon-header'/>
                Customer Information
              </div>
              <List dense className={classes.root}>
                <ListItem className='form-field'>
                  <ListItemText className='form-label'  secondary='Invoice #:' />
                  <ListItemText className='form-value' primary={invoice.name} />
                </ListItem>
                <ListItem className='form-field'>
                  <ListItemText className='form-label'  secondary='Project #:' />
                  <ListItemText className='form-value' primary={invoice.appointment.name} />
                </ListItem>
                <ListItem className='form-field'>
                  <ListItemText className='form-label' secondary='Customer Name:' />
                  <ListItemText className='form-value' primary={invoice.appointment.customerName.first + ' ' + invoice.appointment.customerName.last}  />
                </ListItem>
                <ListItem className='form-field'>
                  <ListItemText className='form-label' secondary='Street Address:' />
                  <ListItemText className='form-value' primary={invoice.appointment.addressLine1} />
                </ListItem>
                <ListItem className='form-field'>
                  <ListItemText className='form-label' secondary='Apt/Suite# :' />
                  <ListItemText className='form-value' primary={invoice.appointment.addressLine2} />
                </ListItem>
                <ListItem className='form-field'>
                  <ListItemText className='form-label' secondary='Zipcode:' />
                  <ListItemText className='form-value' primary={invoice.appointment.zipCode} />
                </ListItem>
                <ListItem className='form-field'>
                  <ListItemText className='form-label' secondary='Phone:' />
                  <ListItemText className='form-value' primary={invoice.appointment.phoneNumber} />
                </ListItem>
                <ListItem className='form-field'>
                  <ListItemText className='form-label' secondary='Email:' />
                  <ListItemText className='form-value' primary={invoice.appointment.email} />
                </ListItem>
                <ListItem className='form-field'>
                  <ListItemText className='form-label' secondary='Start Date:' />
                  <ListItemText className='form-value' primary={(new Date(invoice.appointment.date)).toDateString()} />
                </ListItem>
              </List>
            </Paper>
          </Grid>
         
          <Grid item md={12} className={classes.container}>
            <Paper square={true}>
              <div className='container vertical-padding' >
                 <div className='invoice-section-header'>
                    <DescriptionIcon className='icon-header' />
                    Mr Repairman Work Agreement
                  </div>
                  <div className='vertical-padding'>
                    <Editor
                      editorState={this.state.agreementContent}
                      toolbarClassName='hide-toolbar'
                      editorClassName="agreement-editor"
                      readOnly={true} 
                      toolbar={this.textToolbarOptions}
                    />
                  </div>
                  <div className='container'>
                    <h6> Customer Signature </h6>
                    <SignatureCanvas ref={this.handleAgreementSignatureRef} penColor='black'
                      canvasProps={{dotSize: 1, className: 'sigCanvas'}} />
                  </div>
                  <div className='container'>
                    <Button disabled={invoice.closed || (typeof invoice.agreementSignatureData !== 'undefined')} variant="contained" color="primary" onClick={this.saveAgreement}>Save & Agree</Button>
                    <Button disabled={invoice.closed || (typeof invoice.agreementSignatureData !== 'undefined')} color="primary" onClick={this.clearAgreementSignature}>Clear</Button>
                  </div>
                </div>
            </Paper>
          </Grid>

          <Grid item sm={12} md={12} className={classes.container}>
            <MaterialTable
              title=""
              columns={this.sessionColumns}
              data={this.state.sessions}
              options={{
                search: false,
                paging: false
              }}
              localization={{
                toolbar: {
                    nRowsSelected: '{0} row(s) selected'
                },
                header: {
                    actions: ''
                },
                body: {
                    emptyDataSourceMessage: '',
                    filterRow: {
                        filterTooltip: 'Filter'
                    }
                }
              }}
              editable={(this.readOnly || invoice.closed )? {} : {
                onRowAdd: (this.readOnly ||  invoice.closed )? null : this.onWorkRecordAdd,
                onRowUpdate: (this.readOnly || invoice.closed )? null: this.onWorkRecordUpdate,
                onRowDelete: (this.readOnly || invoice.closed )? null : (oldData) =>
                  new Promise(resolve => {
                    resolve();
                    const data = [...this.state.sessions];
                    data.splice(data.indexOf(oldData), 1);
                    this.setState({ sessions : data});
                  }),
              }}
               components={{
                  Toolbar: props => (
                    <div className='container'>
                      <Grid container spacing={2}>
                        <Grid item xs={2} sm={1}>
                            <TodayIcon className='icon-header' />
                        </Grid>
                        <Grid item xs={6} sm={4}>
                            <div className='invoice-section-header' >Service Technician Labor Record</div> 
                        </Grid>
                      </Grid>
                      <MTableToolbar {...props} />
                    </div>
                  ),
                }}
            />
          </Grid>
          
          <Grid item md={6} md={6}>
            <Paper className='container vertical-padding' square={true}>
               <div className='invoice-section-header'>
                <AssignmentIcon className='icon-header' />
                Invoice Description
              </div>
                <Editor
                  editorState={this.state.details}
                  editorClassName="work-editor"
                  onEditorStateChange={this.onDetailsChange}
                  toolbarClassName={(this.readOnly || invoice.closed) ? 'hide-toolbar' : 'toolbar-class'}
                  readOnly={this.readOnly || invoice.closed} 
                  toolbar={this.textToolbarOptions}
                />
            </Paper>
          </Grid>
          
          <Grid item sm={12} md={6}>
            <Paper className='container vertical-padding' square={true}>
               <div className='invoice-section-header'>
                <ShoppingCartIcon className='icon-header' />
                Materials Used
              </div>
                <Editor
                  editorState={this.state.materials}
                  editorClassName="work-editor"
                  onEditorStateChange={this.onMaterialsChange}
                  readOnly={this.readOnly || invoice.closed}
                  toolbarClassName={(this.readOnly || invoice.closed) ? 'hide-toolbar' : 'toolbar-class'}
                  toolbar={this.textToolbarOptions}
                />
            </Paper>
          </Grid>

          <Grid item md={6} sm={12} className={classes.container}>
            <Paper square={true}>
              <div className='container vertical-padding' >
                 <div className='invoice-section-header'>
                    <DescriptionIcon className='icon-header' />
                    Cost Breakup
                  </div>
                  <div className='vertical-padding'>
                    <MaterialTable
                    title=""
                    columns={this.costTableColumns}
                    data={this.getCostList(this.state.costs)}
                    options={{
                      search: false,
                      paging: false
                    }}
                    localization={{
                      toolbar: {
                          nRowsSelected: '{0} row(s) selected'
                      },
                      header: {
                          actions: ''
                      },
                      body: {
                          emptyDataSourceMessage: '',
                          filterRow: {
                              filterTooltip: 'Filter'
                          }
                      }
                    }}
                    editable={(this.readOnly || invoice.closed) ? {} : {
                      onRowUpdate: this.onChargeUpdate,
                    }}
                  /> 
                  </div>
                </div>
            </Paper>
          </Grid>

          <Grid item sm={12} md={6} className={classes.container}>
            <Paper square={true}>
              <div className='container vertical-padding' >
                 <div className='invoice-section-header'>
                    <DescriptionIcon className='icon-header' />
                    Payment
                  </div>
                  <div className='vertical-padding'>
                  {payment}
                  </div>
                </div>
            </Paper>
          </Grid>
        </Grid>
      
        <Snackbar
          open={this.state.confirmationSnackbarOpen || this.state.loading}
          message={this.state.loading ? 'Loading... ' :this.state.confirmationSnackbarMessage || ''}
          autoHideDuration={3000}
          action={[
            <IconButton
              key="close"
              aria-label="close"
              color="inherit"
              className={classes.close}
              onClick={() => this.setState({ confirmationSnackbarOpen: false })}>
              <CloseIcon />
            </IconButton>
            ]}
          />
      </Container>);
  }
}

export default withStyles(styles, { withTheme: true })(InvoicePage);
