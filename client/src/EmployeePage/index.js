import React from 'react';
import BigCalendar from 'react-big-calendar';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import axios from 'axios';
import {systemConstants} from '../constants';
import AppointmentPage from './../AppointmentPage';
import InvoicePage from './../InvoicePage';
import moment from 'moment';
import './style.scss';
import AppBar from '@material-ui/core/AppBar';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import Toolbar from '@material-ui/core/Toolbar';
import Scrollchor from 'react-scrollchor';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Logo from './../graphics/BigLogo.svg';
import NavItem from 'react-bootstrap/NavItem';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import extend from 'lodash/extend';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';

const HOST = systemConstants.SERVICE_URL;
let localizer = BigCalendar.momentLocalizer(moment);
let allViews = Object
  .keys(BigCalendar.Views)
  .map(k => BigCalendar.Views[k])

const styles = {
  appBar: {
    position: 'fixed'
  },
  flex: {
    flex: 1
  }
};

const DialogTitle = withStyles(styles)(props => {
  const {children, classes, onClose} = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <Typography variant="h6">{children}</Typography>
      {onClose
        ? (
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={onClose}>
            <CloseIcon/>
          </IconButton>
        )
        : null}
    </MuiDialogTitle>
  );
});

class EmployeePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: [],
      user: null,
      events: [],
      appointmentTitle: '',
      appointmentModalOpen: false,
      invoiceModalOpen: false,
      closedInvoices: [],
      openInvoices: []
    };

    this.hideChat();
  }

  hideChat = () => {
    if (window.drift && window.drift.api) {
      window
        .drift
        .api
        .widget
        .hide();
    }
  }

  componentWillMount() {
    axios
      .get(HOST + 'api/user')
      .then(res => {
        if (res.data === 401) {
          window.location.href = '/login';
        }
        this.setState({user: res.data});
      });

    axios
      .get(HOST + 'api/appointments')
      .then(res => {
        if (res.data === 401) {
          window.location.href = '/login';
        }
        this.setAppointments(res.data);
      });
  }

  invoiceSaveCbk = (cbk) => {
    this.invoiceSave = cbk;
  }

  Transition = (props) => {
    return <Slide direction="up" {...props}/>;
  }

  openAppointment = (appointment) => {
    this.setState({appointment: appointment, appointmentTitle: appointment.title, appointmentModalOpen: true});
  }

  openInvoice = (invoice) => {
    this.setState({invoice: invoice, invoiceModalOpen: true});
  }

  setAppointments = (appointments) => {
    let events = [];
    if (appointments.length > 0) {
      appointments.forEach((appointment) => {
        var start = moment(appointment.date)
          .hour(9)
          .minute(0)
          .add(appointment.slot)
          .toDate();
        var end = new Date(start);
        end.setHours(start.getHours() + 2);
        appointment = extend(appointment, {
          id: appointment._id,
          title: appointment.name + ', ' + appointment.customerName
            ? (appointment.customerName.first + ' ' + appointment.customerName.last)
            : ', ' + appointment.addressLine1,
          start: start,
          end: end,
          appointment: appointment
        });
        events.push(appointment);
        this.setInvoices(appointment.invoices, appointment);
      });
    }
    this.setState({events: events, appointments: appointments});
  }

  setInvoices = (invoices, appointment) => {
    let closedInvoices = this.state.closedInvoices;
    let openInvoices = this.state.openInvoices;

    invoices.forEach((invoice) => {
      invoice.appointment = appointment;
      if (invoice.closed) {
        closedInvoices.push(invoice);
      } else {
        openInvoices.push(invoice);
      }
    });

    this.setState({closedInvoices: closedInvoices, openInvoices: openInvoices});
  }

  render() {
    return (
      <div className='wrapper'>
        <Dialog
          open={this.state.appointmentModalOpen}
          onClose={() => this.setState({appointmentModalOpen: false})}
          TransitionComponent={this.Transition}>
          <DialogTitle onClose={() => this.setState({appointmentModalOpen: false})}>Project Invoices</DialogTitle>
          <DialogContent>
            <AppointmentPage appointment={this.state.appointment}></AppointmentPage>
          </DialogContent>
        </Dialog>

        <Dialog
          fullScreen
          open={this.state.invoiceModalOpen}
          onClose={() => this.setState({invoiceModalOpen: false})}
          TransitionComponent={this.Transition}>
          <AppBar className={this.props.classes.appBar}>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={() => this.setState({invoiceModalOpen: false})}
                aria-label="Close">
                <CloseIcon/>
              </IconButton>
              <Typography variant="h6" color="inherit" className={this.props.classes.flex}>
                Invoice# {this.state.invoice
                  ? this.state.invoice.name
                  : null}
              </Typography>
              <IconButton
                color="inherit"
                onClick={() => this.invoiceSave()}
                aria-label="Close">
                <SaveIcon/>
              </IconButton>
            </Toolbar>
          </AppBar>
          <InvoicePage saveCbk={this.invoiceSaveCbk} invoice={this.state.invoice}></InvoicePage>
        </Dialog>

        <Navbar
          collapseOnSelect
          expand="lg"
          bg="light"
          variant="light"
          fixed="top"
          className='mynavbar'>
          <Navbar.Brand href="/">
            <img src={Logo} className='logo'></img>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <NavItem>
                <Scrollchor to="#sec-calendar" className="nav-link">Calendar</Scrollchor>
              </NavItem>
              <NavItem>
                <Scrollchor to="#sec-invoices" className="nav-link">Invoices</Scrollchor>
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <div className="container">
          <div className="row justify-content-center text-center">
            <div className="col-md-10 col-lg-6">
              <p className="lead">
                <span>Welcome
                </span>
                {this.state.user
                  ? (
                    <span>{this.state.user.name.first} {this.state.user.name.last}</span>
                  )
                  : null}
                .</p>
            </div>
          </div>
        </div>

        <section className='container' id="sec-calendar">
          <div className='row justify-content-center text-center'>
            <div className="col-md-10 col-lg-6">
              <h1 className="display-5 h1">Calendar</h1>
            </div>
          </div>
          <BigCalendar
            localizer={localizer}
            events={this.state.events}
            views={allViews}
            step={15}
            showMultiDayTimes
            timeslots={8}
            onSelectEvent={appointment => this.openAppointment(appointment)}
            defaultView={BigCalendar.Views.MONTH}
            defaultDate={new Date()}/>
        </section>

        <section className='grey-section' id="sec-invoices">
          <div className='container'>
            <div className='row justify-content-center text-center'>
              <div className="col-md-10 col-lg-6">
                <h1 className="display-5 h1">Open Invoices</h1>
              </div>
            </div>
            <div className='row justify-content-center text-center workcategories-row'>
              <Paper className={this.props.classes.paper}>
                <Table className={this.props.classes.table} size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell>Invoice #</TableCell>
                      <TableCell align="right">Customer Name</TableCell>
                      <TableCell align="right">Project #</TableCell>
                      <TableCell align="right">Amount</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this
                      .state
                      .openInvoices
                      .map(invoice => (
                        <TableRow key={invoice.name} onClick={event => this.openInvoice(invoice)}>
                          <TableCell component="th" scope="row">
                            {invoice.name}
                          </TableCell>
                          <TableCell align="right">{invoice.appointment.customerName.first} {invoice.appointment.customerName.last}
                          </TableCell>
                          <TableCell align="right">{invoice.appointment.name}</TableCell>
                          <TableCell align="right">{invoice.totalAmount}</TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </Paper>
            </div>
          </div>
        </section>

        <section className='grey-section' id="sec-calendar">
          <div className='container'>
            <div className='row justify-content-center text-center'>
              <div className="col-md-10 col-lg-6">
                <h1 className="display-5 h1">Closed Invoices</h1>
              </div>
            </div>
            <div className='row justify-content-center text-center workcategories-row'>
              <Paper className={this.props.classes.paper}>
                <Table className={this.props.classes.table} size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell>Invoice #</TableCell>
                      <TableCell align="right">Customer Name</TableCell>
                      <TableCell align="right">Project #</TableCell>
                      <TableCell align="right">Amount</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this
                      .state
                      .closedInvoices
                      .map(invoice => (
                        <TableRow key={invoice.name} onClick={event => this.openInvoice(invoice)}>
                          <TableCell component="th" scope="row">
                            {invoice.name}
                          </TableCell>
                          <TableCell align="right">{invoice.appointment.customerName.first} {invoice.appointment.customerName.last}
                          </TableCell>
                          <TableCell align="right">{invoice.appointment.name}</TableCell>
                          <TableCell align="right">{invoice.totalAmount}</TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </Paper>
            </div>
          </div>
        </section>

      </div>
    );
  }

}

EmployeePage.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, {withTheme: true})(EmployeePage);
