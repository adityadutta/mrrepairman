import React from 'react';
import BigCalendar from 'react-big-calendar';
import axios from 'axios';
import { systemConstants } from '../constants';
import { AppointmentPage } from './../AppointmentPage';
import moment from 'moment';
import './style.scss';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Slide from '@material-ui/core/Slide';

const HOST = systemConstants.SERVICE_URL ;
BigCalendar.momentLocalizer(moment);
let allViews = Object.keys(BigCalendar.Views).map(k => BigCalendar.Views[k])

export class EmployeePage extends React.Component {
 	constructor(props) {
    super(props);
    this.state = {
      appointmentTitle: '',
      appointmentModalOpen: false
    };

    this.customContentStyle = {
      width: '100%',
      height: '100%',
      maxWidth: 'none'
    };

    this.modalActions = [
      <FlatButton
        label="Close"
        secondary={true}
        onClick={() => this.setState({ appointmentModalOpen : false})} />,
    ];
	}

  componentWillMount() {
    axios.get(HOST + 'api/calendar').then(res => {
        if (res.data === 401){
          window.location.href = '/login';
        }
        this.setEvents(res.data.data);
    });
  }

  openAppointment = (event) => {
    this.setState({event: event, appointmentTitle: event.title,  appointmentModalOpen: true});
  }

  setEvents = (events) => {
    let items = [];
    if (events.length > 0) {
      events.forEach((item, i) => {
        var start = moment(item.date).hour(9).minute(0).add(item.slot).toDate();
        var end = new Date(start);
        end.setHours(start.getHours() + 2);

        var event = {
          id: item._id ,
          title:item.name.first + ' ' +  item.name.last + ', ' + item.addressLine1,
          start: start,
          end: end,
          data: item
        }
        items.push(event);
      });
    }
    this.setState({events: items});
  }

  Transition = (props) => {
    return <Slide direction="up" {...props} />;
  }

  render() {
    if (!this.state.events){
      return null;
    }
		return (
      <div className='calendar'>

        <Dialog
          open={this.state.appointmentModalOpen}
          onClose={() => this.setState({ appointmentModalOpen: false })}
          TransitionComponent={this.Transition}
          >
          <AppointmentPage event={this.state.event}></AppointmentPage>
        </Dialog>

        <BigCalendar
          events={this.state.events}
          views={allViews}
          step={15}
          showMultiDayTimes
          timeslots={8}
          onSelectEvent={event => this.openAppointment(event)}
          defaultView={BigCalendar.Views.WEEK}
          defaultDate={new Date()}
        />
    </div>
    );
	}
}


