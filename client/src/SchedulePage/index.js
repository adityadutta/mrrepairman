import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import MenuItem from '@material-ui/core/MenuItem';
import Snackbar from '@material-ui/core/Snackbar';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import async from 'async';
import axios from 'axios';
import moment from 'moment';
import { systemConstants } from '../constants';
import DropIn from "braintree-web-drop-in-react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MaterialSelect from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Checkbox from '@material-ui/core/Checkbox';
import { Autocomplete } from '@material-ui/lab';
import 'bootstrap/scss/bootstrap-grid.scss';
import './style.scss';
const HOST = systemConstants.SERVICE_URL ;

 class SchedulePage extends React.Component {
    constructor(props) {
        super(props);
        this.brainTree;
        this.state = {
            activeStep: 0,
            appointmentDate: null,
            loading: true,
            paid : false,
            submitted: false,
            failedPayment: false,
            navOpen: false,
            appointmentSlot: -1,
            confirmationModalOpen: false,
            confirmationTextVisible: false,
            stepIndex: 0,
            appointmentDateSelected: false,
            appointmentMeridiem: 0,
            validFirstName: false,
            validLastName: false,
            validEmail: false,
            validPhone: false,
            zipCode: {
              name : ""
            },
            validAddressLine1: false,
            validAddressLine2: false,
            smallScreen: window.innerWidth < 768,
            confirmationSnackbarOpen: false,
            workCategories: [],
            config : {
              workCategories: [], 
              zipCodes : []
            },
            completedSteps: {},
            zipResult : (<div></div>),
            map : (<div></div>),
            tickIndicator: (<div></div>)
        };

        this.outsideServiceArea = (
        <div className='container'>
          <p></p>
          <div className='outsideservicearea'>Sorry, this is outside our service area</div>
        </div>);

       this.insideServiceArea = (
        <div className='container'>
          <p></p>
          <div className='insideservicearea'>Great, this is within our service area. Please proceed by clicking NEXT</div>
        </div>);


        this.getClientToken();

        this.hideChat();

        this.prePayDialogActions = (<DialogActions>
                <Button onClick={this.handleCancel} color="primary">
                  Cancel
                </Button>
                <Button onClick={this.handlePay} color="primary">
                  Confirm and Pay
                </Button>
              </DialogActions>);

        this.failPayDialogActions = (<DialogActions>
          <Button onClick={this.handleCancel} color="primary">
            OK
          </Button>
        </DialogActions>);
    }

    getStep = (step) => {
      switch(step) {
        case 0:
          return { 
            handleNext: this.handleNext,
            title: '',
            disableNext: () => {
              return !this.state.zipCodeValid || this.state.outsideServiceArea || !this.state.locationType || !this.state.workCategories.length
            },
            div: (
            <React.Fragment>
              <h5>Pay service fee of ${this.state.serviceFee}. Minimum duration is 2 hours charged at hourly rates and invoiced at job completion.</h5>
                            
              <div className="schedule-step-title">Please enter your zip code</div>
              <Autocomplete
                options={this.state.config.zipCodes}
                getOptionLabel={option => option.name}
                onChange={this.handleZipCodeChange}
                value={this.state.zipCode}
                renderInput={params => (
                  <TextField {...params} variant="outlined" className="zipcode" />
                )}
              />
              <div className="schedule-step-title">Choose type/s of service</div>
              <FormControl className="form-element">
                <Autocomplete
                    multiple
                    id="tags-standard"
                    options={this.state.config.workCategories}
                    getOptionLabel={option => option.label}
                    onChange={this.handleWorkCategoryChange}
                    value={this.state.workCategories}
                    renderInput={params => (
                      <TextField
                        {...params}
                        variant="standard"
                        placeholder="Add work types"
                        className="servicetypes"
                      />
                    )}
                  />
              </FormControl> 

              <div className="schedule-step-title">What kind of location is this?</div>
              <FormControl className="form-element">
                <RadioGroup
                    aria-label="Location Type"
                    name="locationType"
                    className="location-type"
                    value={this.state.locationType}
                    onChange={this.handleSetLocationType}
                  >
                  <FormControlLabel value="residence" control={<Radio />} label="Residence" />
                  <FormControlLabel value="commercial" control={<Radio />} label="Commercial" />
                </RadioGroup>
              </FormControl>
            </React.Fragment>
          )};
       
      case 1:
        return {
          handleNext: this.handleNext,
          title: 'Choose an available day and time for your appointment',
          disableNext: () => {
            return !this.state.appointmentDate || !(this.state.appointmentSlot+1)
          },
          div: (<React.Fragment>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      margin="normal"
                      shouldDisableDate={this.checkDisableDate}
                      placeholder="Click here to select date"
                      format="MM/dd/yyyy"
                      value={this.state.appointmentDate}
                      onChange={this.handleSetAppointmentDate}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </MuiPickersUtilsProvider>
                    <form autoComplete="off">
                    <FormControl>
                      <MaterialSelect
                        value={this.state.appointmentMeridiem}
                        onChange={this.handleSetAppointmentMeridiem}
                        inputProps={{
                          name: 'appm',
                          id: 'appm',
                        }}
                      >
                        <MenuItem value={0}>AM</MenuItem>
                        <MenuItem value={1}>PM</MenuItem>
                      </MaterialSelect>
                    </FormControl>
                    <FormControl>
                      <RadioGroup
                          aria-label="Appointment Times"
                          name="appointmentTimes"
                          className={this.props.classes.group}
                          value={this.state.appointmentSlot}
                          onChange={this.handleSetAppointmentSlot}
                        >
                      {this.renderAppointmentTimes()}
                    </RadioGroup>
                  </FormControl>
                </form>
                </React.Fragment>
          )
        };
        case 2:
          return {
            handleNext: this.handleNext,
            title: "",
            disableNext: () => {
              return !this.isPersonalDetailsValid() || !this.isAddressFormValid()
            },
            div: (<div className='container-fluid'>
                    <div className='row'>
                      <div className='col-md-12'>
                        <TextField
                          id="first_name"
                          name="first_name"
                          className="textField"
                          label="First Name"
                          value={this.state.firstName}
                          autoComplete="given-name"
                          error={!this.state.validFirstName}
                          onChange={this.validateFirstName}/>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-12'>
                        <TextField
                          id="last_name"
                          name="last_name"
                          className="textField"
                          label="Last Name"
                          value={this.state.lastName}
                          autoComplete="family-name"
                          error={!this.state.validLastName}
                          onChange={this.validateLastName}/>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-12'>
                        <TextField
                          id="phone"
                          name="phone"
                          className="textField"
                          label="Phone"
                          autoComplete="mobile"
                          value={this.state.phoneNumber}
                          error={!this.state.validPhone}
                          onChange={this.validatePhone} />
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-12'>
                        <TextField
                        id="email"
                        name="email"
                        label="Email"
                        type="email"
                        value={this.state.email}
                        autoComplete="email"
                        className="textField"
                        error={!this.state.validEmail}
                        onChange={this.validateEmail}/>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-12'>
                        <TextField
                          className="textField"
                          id="streetName"
                          name="streetName"
                          label='Street Address'
                          value={this.state.addressLine1}
                          autoComplete="street-address"
                          error={!this.state.validAddressLine1}
                          onChange={this.validateAddressLine1} />
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-md-12'>
                        <TextField
                          id="houseApptNumber"
                          name="houseApptNumber"
                          className="textField"
                          label='Appt/Unit #'
                          value={this.state.addressLine2}
                          autoComplete="address-line1"
                          error={!this.state.validAddressLine2}
                          onChange={this.validateAddressLine2} />
                      </div>
                    </div>
                    <div className="schedule-step-title">Please let us you if you have any specific instructions</div>            
                    <TextField
                      style={{ display: 'block' }}
                      fullWidth
                      value={this.state.customerInstructions}
                      inputStyle={{ paddingTop: 30, fontWeight:'bolder', color: 'blue'}}
                      name="instructions"
                      onChange={this.handleInstructionsChange} />
                  </div>)
          };
          
          case 3:
            let payment = (
              <React.Fragment>
                <div>Pay Service Fee : ${this.state.serviceFee}</div>
                <div>Minimum duration is 2 hours charged at hourly rates and invoiced at job completion.</div>
              </React.Fragment>
            );

            if (this.state.paid){
              payment =
              (<div className='conf'>Thank you for your payment of <span className='bold'>${this.state.serviceFee}</span>.
                  Your appointment has been confirmed for <span className='bold'>{moment(this.state.appointment.date).format('dddd[,] MMMM Do')}</span> at
                  <span className='bold'> {moment().hour(9).minute(0).add(this.state.appointment.slot, 'hours').format('h:mm a')}</span>. You confirmation number is <span className='bold'>{this.state.appointment.name}</span> and
                  a confirmation email has been sent to your mail <span className='bold'>{this.state.appointment.email}</span>. We look forward to see you then!
              </div>);
            }
            return {
              title: '',    
              handleNext: () => this.openPaymentConfirmation(),
              pay: true,
              disableNext: () => false,
              div: (<React.Fragment>
                  {payment}
                  <div className='dropin'>
                    <DropIn
                      options={{ authorization: this.state.clientToken }}
                      onInstance={instance => (this.brainTree = instance)}
                    />
                  </div>
            </React.Fragment>)
          };
        }
    }

    openPaymentConfirmation = () => {
      this.setState({confirmationModalOpen : true});
    }

    hideChat = () => {
      if (window.drift && window.drift.api){
        window.drift.api.widget.hide();
      }
    }

    handleNext = () => {
      this.setState(prevState => ({
        activeStep: prevState.activeStep + 1,
      }));
    };

    handleBack = () => {
      this.setState(prevState => ({
        activeStep: prevState.activeStep - 1,
      }));
    };

    handleChange = (event) => {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    test = (args, hh) => {
      console.log(args);
    }

    getNextBusinessDay = () => {
      var day = new Date();
      let delta = 1 ;
      if (day.getDay() === 5) {
        delta = 3 ;
      }
      else if (day.getDay() === 6){
        delta = 2 ;
      }
      day.setDate(day.getDate() + delta);
      return day;
    }

    /* scheduler code */
    handleNavToggle = () => {
      return this.setState({ navOpen: !this.state.navOpen })
    }

    handleNextStep = () => {
      const { stepIndex } = this.state
      this.state.completedSteps[stepIndex] = true;
      return (stepIndex < 4) ? this.setState({ stepIndex: stepIndex + 1, completedSteps: this.state.completedSteps }) : null
    }

    handleSetAppointmentDate = (date) => {
      this.handleNextStep();
      this.setState({ appointmentDate: date, confirmationTextVisible: true })
    }

    handleSetAppointmentSlot = (event) => {
      this.handleNextStep();
      this.setState({ appointmentSlot: parseInt(event.target.value) })
    }

    handleSetLocationType = (event) => {
      this.handleNextStep()
      this.setState({ locationType: event.target.value })
    }

    handleSetAppointmentMeridiem = (event) => {
      this.setState({ appointmentMeridiem: event.target.value})
    }

    handleFetch = (response) => {
      const { config, appointments, serviceFee } = response
      const initSchedule = {}
      const today = moment().startOf('day')
      initSchedule[today.format('YYYY-MM-DD')] = true
      let schedule = [];
      if (Array.isArray(appointments)) {
        schedule = !appointments.length ? initSchedule : appointments.reduce((currentSchedule, appointment) => {
          const { date, slot } = appointment
          const dateString = moment(date, 'YYYY-MM-DD').format('YYYY-MM-DD')
          !currentSchedule[date] ? currentSchedule[dateString] = Array(8).fill(false) : null
          Array.isArray(currentSchedule[dateString]) ?
            currentSchedule[dateString][slot] = true : null
          return currentSchedule
        }, initSchedule);
      }

      for (let day in schedule) {
        let slots = schedule[day]
        slots.length ? (slots.every(slot => slot === true)) ? schedule[day] = true : null : null
      }
      
      this.setState({
        schedule,
        config : {
          workCategories: (Array.isArray(config.workCategories) ? config.workCategories:[]).map((item, idx)=>{
            return {label: item.name, value: item.name}
          }), 
        zipCodes : (Array.isArray(config.zipCodes) ? config.zipCodes : [] ).map((item)=>{
          return {name: item.name.toString(), description: item.description}
        })
        },
        serviceFee: serviceFee,
        loading: false
      })
    }

    handleFetchError = (err) => {
      console.log('Error fetching data:' + err)
      this.setState({ confirmationSnackbarMessage: 'Error fetching data', confirmationSnackbarOpen: true })
    }

    validateEmail = (event) => {
      let email = event.target.value;
      const regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
      return regex.test(email) ? this.setState({ email: email, validEmail: true }) : this.setState({ validEmail: false })
    }

    validatePhone = (event) => {
      let phoneNumber = event.target.value;
      const regex = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/
      return regex.test(phoneNumber) ? this.setState({ phoneNumber: phoneNumber, validPhone: true }) : this.setState({ phoneNumber: phoneNumber, validPhone: false })
    }

    validateFirstName = (event) => {
      let name = event.target.value;
      if ((name.length > 1)) {
        this.setState({ firstName: name, validFirstName: true });
      }
      else {
        this.setState({ firstName: name, validFirstName: false });
      }
    }

    validateLastName = (event) => {
      let name = event.target.value;
      if ((name.length > 2)) {
        this.setState({ lastName: name, validLastName: true });
      }
      else {
        this.setState({ lastName: name, validLastName: false });
      }
    }

    validateAddressLine1 = (event) => {
      let addressLine = event.target.value;
      if ((addressLine.length > 5) && (addressLine.indexOf(' ') > -1 )) {
        this.setState({ addressLine1: addressLine, validAddressLine1: true });
      }
      else {
        this.setState({ addressLine1: addressLine, validAddressLine1: false })
      }
    }

    validateAddressLine2 = (event) => {
      let addressLine = event.target.value;
      if (addressLine.length > 0) {
        this.setState({ addressLine2: addressLine, validAddressLine2: true });
      }
      else {
        this.setState({ addressLine2: addressLine, validAddressLine2: false })
      }
    }

    checkDisableDate = (day) => {
      const dateString = moment(day).format('YYYY-MM-DD')
      return this.state.schedule[dateString] || moment(day).startOf('day').diff(moment().startOf('day')) < 0 || day.getDay() === 0 || day.getDay() === 6
    }

    renderConfirmationString = () => {
      const spanStyle = {color: '#00bcd4'}
      return this.state.confirmationTextVisible ? <h3 style={{ textAlign: this.state.smallScreen ? 'center' : 'left', color: '#bdbdbd', lineHeight: 1.5, padding: '0 10px', fontFamily: 'Montserrat'}}>
        { <span>
          Scheduling appointment {this.state.appointmentDate && <span>
            on <span style={spanStyle}>{moment(this.state.appointmentDate).format('dddd[,] MMMM Do')}</span>
        </span>} {Number.isInteger(this.state.appointmentSlot) && <span>at <span style={spanStyle}>{moment().hour(9).minute(0).add(this.state.appointmentSlot, 'hours').format('h:mm a')}</span></span>}
        </span>}
      </h3> : null
    }

    isSlotTaken = (appointmentDateString, slot) => {
      return this.state.schedule[appointmentDateString] ? true : false;
      /*
      if (!this.state.schedule[appointmentDateString] ){
        return false;
      }
      let slots = this.state.schedule[moment(this.state.appointmentDate).format('YYYY-DD-MM')];
      if ((slots[slot]) || (slots[slot + 1])){
        return true;
      }
      return false;
      */
    }

    renderAppointmentTimes() {
      if (!this.state.loading) {
        const slots = [...Array(8).keys()]
        let checkboxes = [];
        slots.forEach(slot => {
          const appointmentDateString = moment(this.state.appointmentDate).format('YYYY-MM-DD')
          const t1 = moment().hour(9).minute(0).add(slot, 'hours')
          const scheduleDisabled = this.isSlotTaken(appointmentDateString, slot);
          const meridiemDisabled = this.state.appointmentMeridiem ? t1.format('a') === 'am' : t1.format('a') === 'pm'
          if (!scheduleDisabled && !meridiemDisabled){
            checkboxes = checkboxes.concat(<FormControlLabel
              value={slot}
              label={t1.format('h:mm a')}
              control={<Radio checked={slot === this.state.appointmentSlot}/>}/>);
          }
        });
        return checkboxes;
      } else {
        return (<div></div>);
      }
    }

    handleWorkCategoryChange = (event, categories) => {
      this.setState({workCategories: categories}  );
    }

    addWorkCategory = (event, flag) => {
      let name = event.target.value;
      if (flag && this.state.workCategories.indexOf(name) === -1){
        this.state.workCategories.push(name);
        this.setState({workCategories: this.state.workCategories});
      }
      else if (!flag && this.state.workCategories.indexOf(name) > -1){
        this.state.workCategories.splice(this.state.workCategories.indexOf(name), 1);
        this.setState({workCategories: this.state.workCategories});
      }
   }

   isWorkCategoryAdded = (name) => {
      return (this.state.workCategories.indexOf(name) > -1);
    }

    renderWorkCategories = () => {
      if (!this.state.loading) {
       let checkboxes = (this.state.config.workCategories.map(category => {
         return <FormControlLabel className='col-md-6'
         control={<Checkbox
           checked={this.isWorkCategoryAdded(category.name)}
           onChange={this.addWorkCategory}
           value={category.name}
         />}
         label={category.name}
         />
       }));
       return (<div>{checkboxes}</div>);
     }
       return (<div></div>);
     }

    renderAppointmentConfirmation = () => {
      const spanStyle = { color: '#00bcd4' }
      return <section>
        <p>Name: <span style={spanStyle}>{this.state.firstName} {this.state.lastName}</span></p>
        <p>Phone: <span style={spanStyle}>{this.state.phoneNumber}</span></p>
        <p>Email: <span style={spanStyle}>{this.state.email}</span></p>
        <p>Location: <span style={spanStyle}>{this.state.addressLine1}, {this.state.addressLine2}, ZIP: {this.state.zipCode.name.toString()}</span></p>
        <p>Appointment: <span style={spanStyle}>{moment(this.state.appointmentDate).format('dddd[,] MMMM Do[,] YYYY')}</span> at <span style={spanStyle}>{moment().hour(9).minute(0).add(this.state.appointmentSlot, 'hours').format('h:mm a')}</span></p>
        <p>Amount: <span style={spanStyle}>${this.state.serviceFee}.00</span></p>
      </section>
  }

    componentWillMount() {
      async.series({
        config(callback) {
          axios.get(HOST + 'api/config').then(res => {
            callback(null, res.data)
          })
        },
        serviceFee(callback) {
          axios.get(HOST + 'api/refdata/service-fee').then(res => {
            callback(null, res.data[0].value)
          })
        },
        appointments(callback) {
          axios.get(HOST + 'api/futureappointments').then(res => {
            callback(null, res.data)
          })
        }
      }, (err,response) => {
        err ? this.handleFetchError(err) : this.handleFetch(response)
      })
    }

    handleZipCodeChange = (event, value) => {
      let zipCode = value.name.toString();
      this.setState({zipCode: value});
      
      if (zipCode.length > 5) {
        zipCode = zipCode.substr(0,5);
        this.setState({zipCode: zipCode});
      }
      if (zipCode.length === 5) {     
          let map = this.state.map;
          let tickIndicator = this.state.tickIndicator;

          let response = {
            data: {  
            }
          };
          // service all zipcodes
          response.data.service = true;

          if (response.data.service){
            tickIndicator = this.insideServiceArea;
          }
          else {
            tickIndicator = this.outsideServiceArea;
          }

          let zipResult =(
            <div className='container justify-content-center text-center'>
                {map}
                {tickIndicator}
            </div>);

          this.setState({zipResult : zipResult, zipCodeValid: true, map: map, outsideServiceArea: !response.data.service, tickIndicator: tickIndicator});
       // });
       
      }
      else {
        this.setState({zipCodeValid: false});
      }
    }

    handleInstructionsChange = (event) => {
      this.setState({customerInstructions: event.target.value});
    }

    updateCheck = () => {
      this.setState((oldState) => {
        return {
          checked: !oldState.checked,
        };
      });
    }

    getClientToken = () => {
      axios.get(HOST + 'api/client_token').then(res => {
        this.setState({clientToken: res.data});
      });
    }

    handlePay = () => {
      this.brainTree.requestPaymentMethod((requestPaymentMethodErr, payload) => {
        // Submit payload.nonce to your server
        axios.post(HOST + 'api/checkout/appointment', { paymentMethodNonce: payload.nonce,
          user : {
            email: this.state.email, firstName: this.state.firstName, lastName: this.state.lastName,
            phoneNumber: this.state.phoneNumber, zipCode : this.state.zipCode.name.toString(), locationType: this.state.locationType,
            workCategories: this.state.workCategories.map(category => {return category.value}), appointmentDate: this.state.appointmentDate.getTime(), appointmentSlot: this.state.appointmentSlot,
            addressLine1: this.state.addressLine1, addressLine2 : this.state.addressLine2
          }})
        .then(response => {
          this.brainTree.teardown((teardownErr) => {
            if (teardownErr) {
              console.error('Could not tear down Drop-in UI!');
            } else {
              console.info('Drop-in UI has been torn down!');
              this.setState({paid: true, appointment: response.data.appointment});
            }
          });
          this.setState({ confirmationSnackbarMessage: "Payment succesfull.", confirmationSnackbarOpen: true })
        })
        .catch(err => {
          console.log(err)
          return this.setState({ failedPayment: true, confirmationSnackbarMessage: "Payment unsuccessfull.", confirmationSnackbarOpen: true })
        })
      });
      this.setState({confirmationModalOpen : false});
    }

    isPersonalDetailsValid = () => {
      return this.state.validFirstName && this.state.validLastName && this.state.validPhone ;
    }

    isAddressFormValid = () => {
      return  this.state.validAddressLine1 && this.state.validEmail;
    }

   isEmailValid = () => {
      return this.state.validEmail ;
    }

    handleCancel = () => {
      this.setState({confirmationModalOpen : false});
    }

    ackFailedPayment = () => {
      this.setState({failedPayment: false});
    }

    render() {
      const { classes, theme } = this.props;
      const { activeStep } = this.state;
      const maxSteps = 4;
      const step = this.getStep(activeStep);
      let title = (<div></div>);
      if (step.title) {
        title = (<Paper square elevation={0} className={classes.header}>
                      <div className='schedule-step-title'>{step.title}</div>
                    </Paper>);
      }

      return (
        <div className='wrapper'>
          <div className='step'>
            {title}
            {step.div}
          </div>
          <MobileStepper
            steps={maxSteps}
            position="static"
            activeStep={activeStep}
            className={classes.mobileStepper}
            nextButton={
              <Button color="primary" variant="contained" size="small" onClick={step.handleNext} disabled={this.state.paid || step.disableNext()}>
                {step.pay ? 'Pay' : 'Next'}
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
              </Button>
            }
            backButton={
              <Button size="small" onClick={this.handleBack} disabled={activeStep === 0 || this.state.paid}>
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                Back
              </Button>
            }
          />

        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="xs"
          aria-labelledby="confirmation-dialog-title"
          open={this.state.confirmationModalOpen}
        >
          <DialogTitle id="confirmation-dialog-title">Confirm appointment and pay</DialogTitle>
          <DialogContent>
            {this.renderAppointmentConfirmation()}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCancel} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handlePay} color="primary">
              Confirm and Pay
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="xs"
          aria-labelledby="confirmation-dialog-title"
          open={this.state.failedPayment}
        >
          <DialogTitle id="confirmation-dialog-title">Payment failed</DialogTitle>
          <DialogContent>
            Unfortunately your payment did not go through. Please try again with correct card information or contact Mr Repairman Support Chat.
          </DialogContent>
          <DialogActions>
            <Button onClick={this.ackFailedPayment} color="primary">
              OK
            </Button>
          </DialogActions>
        </Dialog>


        <Snackbar
          open={this.state.confirmationSnackbarOpen || this.state.loading}
          message={this.state.loading ? 'Loading... ' :this.state.confirmationSnackbarMessage || ''}
          autoHideDuration={10000}
          onClose={() => this.setState({ confirmationSnackbarOpen: false })} />
        </div>
      );

    }
}

SchedulePage.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles({}, { withTheme: true })(SchedulePage);
