import React from "react";
import DropIn from "braintree-web-drop-in-react";

class Payment extends React.Component {
	instance;

	state = {
		clientToken: 'sandbox_4s58psps_vc4tb34c9q73b5pk'
	};

	async componentDidMount() {
		// Get a client token for authorization from your server
    /*
    const response = await fetch("server.test/client_token");
		const clientToken = await response.json(); // If returned as JSON string

		this.setState({
			clientToken
    });
    */
	}

	async buy() {
		// Send the nonce to your server
		const { nonce } = await this.instance.requestPaymentMethod();
		await fetch(`server.test/purchase/${nonce}`);
	}

	render() {
		if (!this.state.clientToken) {
			return (
				<div>
					<h1>Loading...</h1>
				</div>
			);
		} else {
			return (
				<div>
					<DropIn
						options={{ authorization: this.state.clientToken }}
						onInstance={instance => (this.instance = instance)}
					/>
					<button onClick={this.buy.bind(this)}>Pay</button>
				</div>
			);
		}
	}
}

export {Payment as Payment};
