import React from "react";
import { compose, withProps } from "recompose";
import { Polygon , withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const Map = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAH77mPR-2aStJulAobgy7Cew8IBvJJpHU&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `150px`, width: `250px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>
  <GoogleMap
    defaultZoom={12}
    defaultCenter={props.center}
    defaultOptions={{
      streetViewControl: false,
      scaleControl: false,
      mapTypeControl: false,
      panControl: false,
      zoomControl: false,
      rotateControl: false,
      fullscreenControl: false
    }}
  >
    <Polygon
      paths={props.paths}
      strokeColor= 'rgb(10, 240, 180)'
      strokeOpacity='0.8'
      strokeWeight='1'
      fillColor= 'rgb(10, 240, 180)'
      fillOpacity='0.35'
    />
  </GoogleMap>
)


export {Map as Map};
