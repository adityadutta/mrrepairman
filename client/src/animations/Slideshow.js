export default class Slideshow {
    constructor(el) {
        this.DOM = {el: el};
        this.DOM.slides = Array.from(this.DOM.el.querySelectorAll('.slide'));
        this.slidesTotal = this.DOM.slides.length;
        this.current = 0;
        this.uncoverItems = [];
        this.DOM.slides.forEach((slide,pos) => this.uncoverItems.push(new Uncover(slide.querySelector('.slide__img'), uncoverOpts[pos])));
        this.init();
    }

    init = () => {
        this.isAnimating = true;
        this.DOM.slides[this.current].classList.add('slide--current');
        this.uncoverItems[this.current].show(true, {
            image: {
                duration: 800,
                delay: 350,
                easing: 'easeOutCubic',
                scale: [1.3,1]
            }
        }).then(() => this.isAnimating = false);
    }

    navigate = (pos) => {
        if ( this.isAnimating || this.current === pos || pos < 0 || pos > this.slidesTotal - 1 ) return;
        this.isAnimating = true;

        this.uncoverItems[this.current].hide(true).then(() => {
            this.DOM.slides[this.current].classList.remove('slide--current');
            this.current = pos;

            const newItem = this.uncoverItems[this.current];
            newItem.hide();
            this.DOM.slides[this.current].classList.add('slide--current');
            newItem.show(true, {
                image: {
                    duration: 800,
                    delay: 350,
                    easing: 'easeOutCubic',
                    scale: [1.3,1]
                }
            }).then(() => this.isAnimating = false);
        });
    }
}