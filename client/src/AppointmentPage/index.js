import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import InvoicePage from './../InvoicePage';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Slide from '@material-ui/core/Slide';
import './style.scss';

const styles = {
  appBar: {
    position: 'fixed'
  },
  flex: {
    flex: 1
  }
};

class AppointmentPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      invoiceModalOpen: false,
      closedInvoices: [],
      openInvoices: []
    };
  }

  openInvoice = (invoice) => {
    //this.setState({invoice: invoice, invoiceModalOpen: true});
    window.open(`/#/invoice/${invoice._id}`, '_blank');
  }

  Transition = (props) => {
    return <Slide direction="up" {...props}/>;
  }

  invoiceSaveCbk = (cbk) => {
    this.invoiceSave = cbk;
  }

  render() {
    return (
      <div className='cointainer'>
        <Dialog
          fullScreen
          open={this.state.invoiceModalOpen}
          onClose={() => this.setState({invoiceModalOpen: false})}
          TransitionComponent={this.Transition}>
          <AppBar className={this.props.classes.appBar}>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={() => this.setState({invoiceModalOpen: false})}
                aria-label="Close">
                <CloseIcon/>
              </IconButton>
              <Typography variant="h6" color="inherit" className={this.props.classes.flex}>
                Invoice# {this.state.invoice
                  ? this.state.invoice.name
                  : null}
              </Typography>

              <IconButton
                color="inherit"
                onClick={() => this.invoiceSave()}
                aria-label="Close">
                <SaveIcon/>
              </IconButton>

            </Toolbar>
          </AppBar>
          <InvoicePage
            readOnly={this.props.readOnly}
            saveCbk={this.invoiceSaveCbk}
            invoice={this.state.invoice}></InvoicePage>
        </Dialog>

        <section></section>

        <section className='grey-section' id="sec-invoices">
          <div className='container'>
            <div className='row justify-content-center text-center'>
              <div className="col-md-10 col-lg-6">
                <h1 className="display-5 h1">Invoices</h1>
              </div>
            </div>
            <div className='row justify-content-center text-center workcategories-row'>
              <Paper className={this.props.classes.paper}>
                <Table className={this.props.classes.table} size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell>Invoice #</TableCell>
                      <TableCell align="right">Amount</TableCell>
                      <TableCell align="right">Closed</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!this.props.appointment.invoices
                      ? null
                      : this
                        .props
                        .appointment
                        .invoices
                        .map(invoice => (
                          <TableRow key={invoice.name} onClick={event => this.openInvoice(invoice)}>
                            <TableCell component="th" scope="row">
                              {invoice.name}
                            </TableCell>
                            <TableCell align="right">{invoice.totalAmount}</TableCell>
                            <TableCell align="right">
                              <Checkbox disabled checked={invoice.closed}/>
                            </TableCell>
                          </TableRow>
                        ))}
                  </TableBody>
                </Table>
              </Paper>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

AppointmentPage.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, {withTheme: true})(AppointmentPage);
