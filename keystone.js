// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config();
var Config = require('./service/Config.js');
// Require keystone
var keystone = require('keystone');
var handlebars = require('express-handlebars');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({
	'name': 'Mr Repairman',
	'brand': 'Mr Repairman',
	'sass': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': '.hbs',

	'custom engine': handlebars.create({
		layoutsDir: 'templates/views/layouts',
		partialsDir: 'templates/views/partials',
		defaultLayout: 'default',
		helpers: new require('./templates/views/helpers')(),
		extname: '.hbs',
	}).engine,

	'emails': 'templates/emails',

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	port: process.env.PORT
	/*
	,
	'ssl port': process.env.SSLPORT,
	ssl: 'force',

	letsencrypt: (process.env.NODE_ENV === 'production') && {
		email: Config.mail.smtp.auth.user,
		domains: ['www.' + Config.domain, Config.domain],
		register: true,
		tos: true,
	}
	*/
});

// Load your project's Models
keystone.import('models');
keystone.set('cloudinary secure', true);
keystone.set('signin logo', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIkAAAAgCAYAAAAmN1vvAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAABLWSURBVHhe7Vt3dJzVlf9NV5clS7bkXiX3XrCNMfaCWzAxh+okZLNZFkjOQswCCZuzhxAcssExxLDLZiEbOosNmDXNmGLLxoUYy92S3OWGLQkjaSSNytT93ftmRjOjEc7Jv8zv6NN83/feu++++259I1k+u3lFyBH0ALDwSiGFTlhCAdRmz4Ll5IJ5oTN5S+AItMhr05rCtxwhBKxpKGrZjksZk2CpuO6W0Mcla5HudSNkSSlJCuJBAK89B+MvroQt2A5L5XduDJUNfRUZvlppNr1S+Najw56L0TVPqUZYzSsBH8WTpK7UFYW5DytJbEMKKcQjxpOkkEJypJQkhcuiq5KEmNrK9U2Itv+VfbXPZfp1AzMqQiN8z+ubqEXbOKa7fvo+ylsSRN7LR3d9CG2Oveumb7RPzJzCXfT95RBHN6F/mKa+jeuXBMnaw2O7GxlT3dTBb0tTtbH7OtjEIXFJTBhC0GKFz5HBfq3sHkTA4oTPmcXG+P6WUJBvfLCRni3oCzd3pSnMBezpnD/dvIgiBGsoAEvQD3uApVgwoMODcMDnymZrEv4IC1vs3mbYQn7W++TNkalvO0G6pCJ0bf42WJVuTLuu0YKgzYmg1QaHt9W8j+tDntnmd2Tro8PrJq9B9rfC68zlm3j7E1mAa7EFRBZePot8KUeu2xrwhuWTfD3CT9BiQ8CRRn7byS9pSZ0qaxI6RMDm4Nwu2P2eMO14WgGrnX1ccHDPlJdIu66VMqXsDS8dcPpb0WHLYXXztMoyqiQ2awdK9z8Ld94w1A6YCyeF3IVpJWhFR1oPzPjopyi/+rfc3EwUXNyFyVt/zo3LNQyIBHl503uirmgWTo+8jUqUqRutC0uAMDek4hUMrXyRQs+Jo+HJHoiL/efh3LAlZIdjyYOrox5XvXcbvDqf6deJEJwdzfj8mv9Cfe/J6H/iHYzc+wcKwdXZl2RaM/uirt9VODXiNt1c2bxYwXkdWehT/TFyG4+iYuqDcLVdMvOH20Xoaa11mPHJnaQLfHrjBxSwX1c3760F8FOJzVT8Jf2dGfi6cApOl94ET48hqpyiwBO2/xqV05ajg2txUAGSyVzmClEhx36+EsfH/ZhyzaOi+E1ftvsd6chqOInSvf+JnYtfQmbLl7pG84vtVJ7CC7vQ+9xW7J/9KFzt9UYW4fEdrh4YeOxNjPv8EVSP+iEqJ91LE0qLKklU3YN2F7Iaz2Ja2X2wcgFBi10JJEKsZAAFP5abKmNE+FnNZ5D3dRXyLlUgv24fr/3I/+ogBhz7ABN2PoxZG/6eAr2kHkeY7gJaSXZDNccf4dhDnTR4Datciys2/zOmf3KPeqsArdvV3si2w+xfpX3ywn3NdQC96irgaq1Xy8tqOYv8S5WGt9q9pk/NPgw++hamli3H1C0PcvEBWqEjul49VOTPoBPrMHHbI+pxgrZ4ecimOb1NpHcIPWv20IJbScNJRWlHYe0h9Kg/hgLl7QBlcQBF1ZsxdvdKzHl/GfJqy2lcabxc6HmxHNM2/0wNTGjGyZz38tSe2Rvjdv07N3IdFcallh8LiQCDj7yO0orXkHvpsPGcsWKmYYshTNz9JEbtWYWWnP5mjeG5xGPmNBxDfsN58lvB9fK9tIfR6RM5wEuNyqWizPr4bjTl9THzRJjmp5+Tp3vOY+rm+xlE+IqbKxABh+ju3D1KsOnGDdh42zZsvHULNi7bjNacvuj15Q4MrnoVfgqmO8iGyuLr+s7AOz86iI9uLcOHy3Zi5/xn0JZehAHV76L/8ffVQsWb+TmfCGPzDe9wnm3aP3Ktv3076osmcuOo7OwniuXOG4GP2G/jrZ9hw/d3onzOKlpQPgYcfxuDjr5J3lyqGLLOAIWeS4UtOlMGv9OJkXueRnt6oWE0Ig8iREPy00OKl5SN4BvDm51hyJaBzxa/TN62qyw++GE5Lvabh0wa1PjPHyPvDM+kJQpQfOZTjPriCbSn9QwT5hzhedozCjDoyDqU7P9veLKK4zZPebU7keU+jb6nP+b4HIwpf5I8Seg37RGIDLwcO2bXSozbsUI9qdk/00cU1s9H2YdEbxYfOEm0Iz2bLvo9TN7yK7RxARGNk3gmglz4+hwVpgSEWIi1Bej2RFE8uYPQmt0fFwfOxb4rVmj873P6k/C4eAZiIXHZm5GPpvxh8OQMRFt2XxyZcCfOliyhldkpiA90TZKLiLsMUkAN+aPRnDsMLT2G8pLPYXwu4cq4NA1bvOWnhMXGnqVozSXdzD44MON+nBzzI+YuHhSd20z330G6RhytWYWYtP1hjvNTHoUYdvh5WtqJ5EpOPiLpgYKfwptsQHPecIYWyoJracovwc6Fz9JDuzQ8u9ov0bDonQjxImN3P67KKqFcICTFcxZc+ILebrlRqrBRRqB9uK6hVa+ooohyFJ0rQyGNUgw6FsJjyGbjenpjwq7HMLnsX2gkOarohlIYMbcRxCsJIflAc95gxvGn0f/kBjIqhJg0UUlmfHQ3Y7dXGU621Uz1KOxWtWA746uDuYEsVBRNE8/wpnUH4U+SU5vfp+Mlbsu9JGSiXFZupPSKrEMTWl+LJn7WgM98so+Fm5sIpesjTeYeDtJ1treioXAMk1umwb5mtotvNBvTs5bhgcJuT8vXEOLqaFRXL/eJVtYdNM9ROcga+EllFKGJxUpu42DOF6T4JeaLAQS4qRO3/Rs9zTmG9Cz1gBbmONPKHlDeJLQnQsYJ/ZL9f9L8rIN75WpvIK9va6LbhVcVMCMCk9TSQ3/ClK0P0TsVqaKooneDLkoiVmpjBgzGx8lbH4CDSY6HFj204gX0O/UeJ5GFJdlsEQAnk6SsMb8frZYWlJmNEQdoPaTVliHM2KRbt5C5Jea68x2kMQgNBQOR3lqDXud3KKNtdLe6IBWsTauSq99dhmvXLca8dUv0Wrh2LobT8rVSi85Gr0Mlb88sJB+96CmKeZ/BRG4Hld8GL928tIucJHyM2fUErc6K2v5zcHT8XVT2RuZX6zUHCWpYuTzEMDxZfdDIsO2mLNyFRRhc8To3lbkLnOShDz2nX9dsdi/AhLsBMzfewQ3PYd5QjOmb7kVufSU3kJ6zi8xDupaSA89R4RvpPQdj97w/UElaMKSS84gSao4T7q7TkAZ/Dk7/V+Vj2OEXMG3T/ZSrpAuSc5muiTD+LgpaPBXkzPDrmS2fQ6/av2Du+pux/8pHMGXLL6g4FlRMeQCDKteE+xvI/H4r43jDcSx9fqS+C0r23/Y1LbuVLjEHlZOXh7N/4SS5qkhlU0zXf/uTo9XVC42MlhpaUocKrmryfVyc/EmDQChZkN5+0XioMFlnWz1cnjoaUedmShWR5T6JqZvuU5cs4wpqyukttmgeVl1yE5VOchcHep8uQ58zGzU/OFNyCyuzRRizexUT8wMYevglHJ76ANLbvgpTTgLyIQos3mzhmnmUGSsTzmflGjJaLnBNVhya/pDhl3zrKiiX2n5/hwxPNZPr/Zi18S6G2/4sENZTWfqhhtVd/5PvGuKESE8S38ym8xhd/gSVvAeOj2dYLl2Imj4z0LNutybkW767BtnuMzomAmdbE3Ys/jn32cexj2PEwWdIL0iFKyDfyQ2gy1txk/J6y9K1aKX15zSeINM/5uIcOD9kCfbN/JW63y6g5chkUhamcWMzm8/rJntdPanhqzUhtQfC5w3fAAkhMjaDZVxaa60mXO78kdi26GXG05503V7ppQIWme2a9x9MEF/B9kUv6FW29G2cGPsP7CflthGqJJP2gIdxfzWm7PgNrxXoV/0hOjILsO/KR5k7Xcsw1MaOIQxmKWjztdETjkRt3yuo5GDZeQc9iAOl+/9IyTA/u4xHFIjAxUjSPBeQ6fmSyu0h/wWomrhc6Tl88odewhs9N3mtK56BvbN/pwrW79T7KN33jFaYVZN+hrNDl2rI6oSF+VEmhh98Tg2kmXlYXZ8r6fV9ODrpJ5RZmipVVuNpKpOcPRk5KNg/nXZWMf0+7J/5a20adOR/MbTy1fD5TkzfMJKqjoNutTm/mAL8DYdQ0xkT3QUjuSGrqe200gRC8iRJp4SU7YtexOYb3yPTM+ie3Zr5nxzzA80ZzDlF9+KVjXLnj2J1VIZNS9erIES49b0m4sKga+lWG6Izi3CCDifq+l2JS31m4iu5imdSGWfTlfdjj4DpSIhVe509sOeqFdg193F8Medx7FzwZ2xe+g6qR34PDs4ripTZdBZ9qzcykcyhon6pYez6l6Yx6d4Ib3o+579Eb/IiOmh1yYQZhSgwy8jyuavw6U0f4sSoH2g+JApxesQtXFeGOecIQ+QpucS54fNQQW+Z1lbHvMnDNc9HxbR7NMzFhhs9o6ERiaLLGYmTKcHsDbfjO6/NZCW2Gr70HuppRu/+veZV8TKnVyPrrvavcXTyT7F31qMc7+Z8UsL/lZ7EwAJXWwCnRn8fh6c/yEcrdnDzvUyMrEFvUvGIB/A5s3GpaAqvqSi/+gl4MouR1XSGcfZuLiZDhRRbliVCaLdlF3PTJ3HzZ7P8/TOVpAMlh/8Hww89z3IwXIYSQkvCVybpZzDZy2iRSzzQOfVAZhYjHBtzlzZWW4emPYQjE36CI5PuwtnhS1nlFJtDQ/Leyvuxf/mtKrZAlDO78STpn2bZX6N8+yjwEXufoiXWcKMk55FZjMnErUqqG27kV5RDY+F4GttjlMl0PZCbtnk5cxy3hrY4cIyrtR2HZvwS1aW3wJ07FNuWvMx3Hso2ovBmPWI8g46vY75SxSerhrKsplNGFlQeMWzx/P2rN7CS2q1KGc8gKYUCzPfqUDn1fsr5WVjpPaVIiMwRi04lkXBBi7NQYWXDhWYGralq4j0ou2E9mnMGwEWNlgaxgljtkkXIpLLJdlpMGvOCFpbBh654mG7ey2x7jVYHqijhMYmQJFQOceQgz+YP0GPVoG7AbFRxU8V9inBz6o9qmS1ziVeSI+R562/A/Deu4bWA13zMf3MBlrw4ibH9MIXDvkFz/G71tjFMNjA5dNOKaDmsiqRSk80JkK9c0h585C1uvoN8/xJl16/DluvXmmvJG9hBz9OeVqinmUOq1qqlisew0oNK/hTxksoX5zQVGis9qZzoBfZcvZL8ZKHXhe0Y+8UqVojZKnPN03Q9comFN2DvVb9juP8/rQ6VNmUj8hG5S/IvdEeVP8WxXhwZe6fuz5YlhtetS9bw8220sPTOdJ+lMr1B4cpuCa+GX6MHxmDTaVDVY25nef6cVlxSLSVqVHSvJRx407I1kw2IOxTmeC+K0ZRXSuaZh3BRIa5ETwYJYVgYF1du95tcJpL8yNHvqZHLcJLu3MVkaQoTKdlUGZPIhCxADnHsJBGwymmhsRwXla1yyv2oK5qpApuy9Rc6n5Spwp9YkN3bouFRNkMvLtRF4YliyIb5WbVYGfNDTLrNSSV/yad4NbkIUd4xu36vwjg/aDH5/h7DbSkaC8bp1dRzFC4OvhbHxv0T1+Whwq+lt6phWcp4L0oRCDB/cCpP5uRWqfJiJch7B3l054/QI3HpP7p8tZ5IS1kbYMiUsl3OfFjb6fqk0pKS2KYhOshkO0PzFi5KE/jBFWuQ21JH7zQNR6fcy8pmKNMBw6u7YAwaek2g91rBMBrE8APPM1ltIF9mz0xprbe6fpGRGOS5Iddh5zV/VIPs9FwGcV/wyZd2cggjB0t6dkACUYhA+WyS0VzKgSGJWm/lwqTc1I3josRC9QszQuK8WEzA7uCC/bqZSlMkpzslkHv5okvmTte5ZbOt4bllUXIuI/NKQilfUMm95A0avqJ0YkDBqlXQs/nIm6xJLEjeycGeQXicrIl0RDhS+Yh30bOXBEHJXOK2RaH0rMXr0XJYvmgUWlKGypeRshnyXYgEIZlPNjoCyc8CNnH9LHfZJt7DvHNxzlY9vxF+4iC8WV0qX8kNRb6SRMu8spmSr4hiJULOQoS28OSkPKl+emgnnlW++zL70CkDgR7YERJqvV2/4HvN/I0rB0VYjCMSC743m8Nm6a0DIlaaMC58r10I06Z3+twJ6WH6aUuUBj/lJ4aPCDVR0m9ClLfw/Ka3GRt5iiJmTYKkaw/zHpk3kY/Is35E5oxdr9wTifN0ypLoZt6/ZYxOaXpE12M46HyOg7yTD76XdvM3rglf8EUgQ3V4IpEIhIh8mAfTz/yYd7HjwvfxbTHtUXT2U0Rp8JP38hS5Inex75Jd+jtmfoPO1jgkzhGdPwb6LmHtSZ7j5gyPUcg9r8gbfRvzrAiPjcPfOEZ6RPuF+yQ+x0He8UrS0lVJUkghESklSeGyCCtJJFqlkEJXxHgSKookL6krdUVh7i0V190c+mT4G0j31cujvkwhBS+rm3H6b55eWI4tXhw60usO1tLu5FlvCt9ChOCzZmBA44doShsGy77v/mPIGjLH0ymkEIGcl8gfip3rsQj/DzLwJ1IOwsanAAAAAElFTkSuQmCC');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});

// Load your project's Routes
keystone.set('routes', require('./routes'));


// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
	/*
	posts: ['posts', 'post-categories'],
	galleries: 'galleries',
	enquiries: 'enquiries',
	*/
	projects: 'appointments',
	finances: ['invoices', 'payments'],
	employees: 'users'
});

// Start Keystone to connect to your database and initialise the web server

if (!process.env.MAILGUN_API_KEY || !process.env.MAILGUN_DOMAIN) {
	console.log('----------------------------------------'
	+ '\nWARNING: MISSING MAILGUN CREDENTIALS'
	+ '\n----------------------------------------'
	+ '\nYou have opted into email sending but have not provided'
	+ '\nmailgun credentials. Attempts to send will fail.'
	+ '\n\nCreate a mailgun account and add the credentials to the .env file to'
	+ '\nset up your mailgun integration');
}

if (keystone.get('env') === 'development'){
	keystone.set('cors allow origin', true);
}

keystone.start();
